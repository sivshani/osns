#ifndef MATH_TEST_HPP
#define MATH_TEST_HPP

namespace math
{
	void test_rad2Deg(unsigned int& test);
	void test_deg2Rad(unsigned int& test);
	void test_sin(unsigned int& test);
	void test_cos(unsigned int& test);
	void test_math(unsigned int& test);
}

#endif /* MATH_TEST_HPP */
