#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <limits>
#include <vector>

#include "boost/multiprecision/mpfr.hpp"
//#include "boost/multiprecision/float128.hpp"

#include <Node_Test.hpp>
#include <Node.hpp>
#include <Orbit_Data.hpp>
#include <Orbit_Test.hpp>
#include <Orbit.hpp>
#include <Math.hpp>
#include <Test.hpp>

using namespace boost::math;
using namespace boost::multiprecision;


void test_node_movment_1(unsigned int& test)
{
	std::cout << "\nTest " << __func__ << ":" << std::endl;

	std::vector<bool> results;

	KOE_MPFR_f_1000 koe_GEO(mpfr_float_1000(Satellites::GEO::radius), mpfr_float_1000(0.0), mpfr_float_1000(0.0), mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GEO (koe_GEO);
	node_GEO.print();

	// GEO Node: Period ~ 86132.0449
	// Movement in intervals of 100 seconds
	float time = 100; // seconds
	for(int i = 0; i < 865; i++)
	{
		std::cout << "test " << test <<": GEO node movement" << std::endl;
		std::cout << "ma: " << node_GEO.getOrbit().mean_anomaly << std::endl;
		std::cout << "sl: " << node_GEO.getSL() << std::endl;

		node_GEO.move(time);

		midResults(test, true, results);

		test++;
	}
	finalResults(results);
	results.clear();


	KOE_MPFR_f_1000 koe_MEO(mpfr_float_1000(Satellites::MEO::radius), mpfr_float_1000(0.0), mpfr_float_1000(0.0), mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_MEO (koe_MEO);
	node_MEO.print();

	// MEO Node: Period ~ 16252.1948
	// Movement in intervals of 100 seconds
	time = 100; // seconds
	for(int i = 0; i < 165; i++)
	{
		std::cout << "test " << test <<": MEO node movement" << std::endl;
		std::cout << "ma: " << node_MEO.getOrbit().mean_anomaly << std::endl;
		std::cout << "sl: " << node_MEO.getSL() << std::endl;

		node_MEO.move(time);

		midResults(test, true, results);

		test++;
	}
	finalResults(results);
	results.clear();


	KOE_MPFR_f_1000 koe_LEO(mpfr_float_1000(Satellites::LEO::radius), mpfr_float_1000(0.0), mpfr_float_1000(0.0), mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_LEO (koe_LEO);
	node_LEO.print();

	// LEO Node: Period ~ 5602.0896
	// Movement in intervals of 100 seconds
	time = 100; // seconds
	for(int i = 0; i < 60; i++)
	{
		std::cout << "test " << test <<": LEO node movement" << std::endl;
		std::cout << "ma: " << node_LEO.getOrbit().mean_anomaly << std::endl;
		std::cout << "sl: " << node_LEO.getSL() << std::endl;

		node_LEO.move(time);

		midResults(test, true, results);

		test++;
	}
	finalResults(results);
	results.clear();
}

void test_node_movment_2(unsigned int& test)
{
	std::cout << "\nTest " << __func__ << ":" << std::endl;

	std::vector<bool> results;

	KOE_MPFR_f_1000 koe_GEO(mpfr_float_1000(Satellites::GEO::radius), mpfr_float_1000(0.0), mpfr_float_1000(0.0), mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GEO (koe_GEO);
	node_GEO.print();
	std::cout << std::endl;

	// GEO Node: Period ~ 86132.0449

	// ---
	// Movement in intervals of period seconds
	std::cout << "test " << test <<": GEO node movement - period seconds" << std::endl;
	std::cout << "ma: " << node_GEO.getOrbit().mean_anomaly << std::endl;
	std::cout << "sl: " << node_GEO.getSL() << std::endl;

	// save start cl position
	CartesianLocation<mpfr_float_1000> cl_start(node_GEO.getCL());
	node_GEO.move(node_GEO.period());

	std::cout << "cl_sta: " << cl_start << std::endl;
	std::cout << "cl_cur: " << node_GEO.getCL() << std::endl;

	// compare start position with current position
	if(node_GEO.getCL().XYZ == cl_start.XYZ) midResults(test, true, results);
	else midResults(test, false, results);

	test++;
	// ---

	node_GEO.setOrbit(node_GEO.getOrbit().radius, node_GEO.getOrbit().inclination, node_GEO.getOrbit().longitude_of_AN, 0);
	// Movement in intervals of 100 period seconds
	std::cout << "test " << test <<": GEO node movement - period seconds * 100" << std::endl;
	std::cout << "ma: " << node_GEO.getOrbit().mean_anomaly << std::endl;
	std::cout << "sl: " << node_GEO.getSL() << std::endl;

	node_GEO.move(node_GEO.period() * 100);

	std::cout << "cl_sta: " << cl_start << std::endl;
	std::cout << "cl_cur: " << node_GEO.getCL() << std::endl;

	// compare start position with current position
	if(node_GEO.getCL().XYZ == cl_start.XYZ) midResults(test, true, results);
	else midResults(test, false, results);

	test++;
	// ---

	node_GEO.setOrbit(node_GEO.getOrbit().radius, node_GEO.getOrbit().inclination, node_GEO.getOrbit().longitude_of_AN, 0);
	// Movement in intervals of half period seconds
	std::cout << "test " << test <<": GEO node movement - period seconds * 0.5" << std::endl;
	std::cout << "ma: " << node_GEO.getOrbit().mean_anomaly << std::endl;
	std::cout << "sl: " << node_GEO.getSL() << std::endl;

	node_GEO.move(node_GEO.period()/2);

	std::cout << "sl_cur: " << node_GEO.getSL() << std::endl;

	if(node_GEO.getSL().Equat.y == RAD_180_M) midResults(test, true, results);
	else midResults(test, false, results);

	test++;
	// ---

	finalResults(results);
	results.clear();
}

void test_node_movment_3(unsigned int& test)
{
	std::cout << "\nTest " << __func__ << ":" << std::endl;

	std::vector<bool> results;

	KOE_MPFR_f_1000 koe_GEO(mpfr_float_1000(Satellites::GEO::radius), RAD_75_M, RAD_135_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GEO (koe_GEO);
	node_GEO.print();
	std::cout << std::endl;

	// ---
	// Movement in intervals of period seconds
	std::cout << "test " << test <<": GEO node movement - period seconds" << std::endl;
	std::cout << "ma: " << node_GEO.getOrbit().mean_anomaly << std::endl;
	std::cout << "sl: " << node_GEO.getSL() << std::endl;

	// save start cl position
	CartesianLocation<mpfr_float_1000> cl_start(node_GEO.getCL());
	node_GEO.move(node_GEO.period());

	std::cout << "cl_sta: " << cl_start << std::endl;
	std::cout << "cl_cur: " << node_GEO.getCL() << std::endl;

	// compare start position with current position
	if(node_GEO.getCL().XYZ == cl_start.XYZ) midResults(test, true, results);
	else midResults(test, false, results);

	test++;
	// ---

	node_GEO.setOrbit(node_GEO.getOrbit().radius, node_GEO.getOrbit().inclination, node_GEO.getOrbit().longitude_of_AN, 0);
	// Movement in intervals of 100 period seconds
	std::cout << "test " << test <<": GEO node movement - period seconds * 100" << std::endl;
	std::cout << "ma: " << node_GEO.getOrbit().mean_anomaly << std::endl;
	std::cout << "sl: " << node_GEO.getSL() << std::endl;

	node_GEO.move(node_GEO.period() * 100);

	std::cout << "cl_sta: " << cl_start << std::endl;
	std::cout << "cl_cur: " << node_GEO.getCL() << std::endl;

	// compare start position with current position
	if(node_GEO.getCL().XYZ == cl_start.XYZ) midResults(test, true, results);
	else midResults(test, false, results);

	test++;
	// ---

	node_GEO.setOrbit(node_GEO.getOrbit().radius, node_GEO.getOrbit().inclination, node_GEO.getOrbit().longitude_of_AN, 0);
	// Movement in intervals of half period seconds
	std::cout << "test " << test <<": GEO node movement - period seconds * 0.5" << std::endl;
	std::cout << "ma: " << node_GEO.getOrbit().mean_anomaly << std::endl;
	std::cout << "sl: " << node_GEO.getSL() << std::endl;

	node_GEO.move(node_GEO.period()/2);

	std::cout << "sl_cur: " << node_GEO.getSL() << std::endl;

	if(node_GEO.getSL().Equat.y == RAD_315_M) midResults(test, true, results);
	else midResults(test, false, results);

	test++;
	// ---


	finalResults(results);
	results.clear();
}

void test_node(unsigned int& test)
{
	std::streamsize ss = std::cout.precision(); // store precision
	std::cout.precision(50); // set precision

//	test_node_movment_1(test);
//	test_node_movment_2(test);
	test_node_movment_3(test);

	std::cout.precision(ss); // restore precision
}

