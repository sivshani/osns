#ifndef ORBIT_TEST_HPP
#define ORBIT_TEST_HPP

void test_orbit(unsigned int& test);
void test_location(unsigned int& test);
void test_keplerianOrbitElements(unsigned int& test);

#endif /* ORBIT_TEST_HPP */
