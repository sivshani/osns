#ifndef MATH_HPP
#define MATH_HPP

#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <limits>
#include <vector>

#include <boost/multiprecision/mpfr.hpp>

#include <Math_Data.hpp>
#include <Test.hpp>
#include <Vectors.hpp>

using namespace boost::math;
using namespace boost::multiprecision;


namespace math
{
	// GEO meterPerRadian = 42367400
	template<typename TReal>
	bool isApproxEqual(TReal a, TReal b, TReal tolerance = TReal(0.0000000001))
	{
		if(a == b) return true;

		TReal diff = fabs(a - b);
		if (diff <= tolerance) return true;
		return false;
	}

	template <class TReal>
	TReal rad2Deg(TReal radians, bool mod2PI=true)
	{
		if(mod2PI) radians = fmod(radians, constants::two_pi<TReal>());
		return radians * (180.0 / constants::pi<TReal>());
	}

	template <class TReal>
	TReal deg2Rad(TReal degrees,  bool mod2PI=true)
	{
		if(mod2PI) degrees = fmod(degrees, 360.0);
		return degrees * (constants::pi<TReal>() / 180.0);
	}

	template <class TReal>
	TReal calc_sin1(TReal rad)
	{
		// 0 <= rad <= 360 (or -360 <= rad <= 0)
		rad = fmod(rad, constants::two_pi<TReal>());

		return sin(rad);
	}

	template <class TReal>
	TReal calc_sin(TReal rad)
	{
		// 0 <= rad <= 360 (or -360 <= rad <= 0)
		rad = fmod(rad, constants::two_pi<TReal>());
		// convert negative rad
		if(rad < 0.0) rad = constants::two_pi<TReal>() + rad;

		// 0 <= rad <= 90
		if(rad <= constants::half_pi<TReal>())
		{
			return sin(rad);
		}
		// 90 <= rad <= 180
		if(rad <= constants::pi<TReal>())
		{
			return sin(constants::pi<TReal>() - rad);
		}
		// 180 <= rad <= 270
		if(rad <= constants::pi<TReal>())
		{
			return -sin(rad - constants::pi<TReal>());
		}
		// 270 <= rad <= 360
		return -sin(constants::two_pi<TReal>() - rad);
	}

	template <class TReal>
	TReal calc_sin2(TReal rad)
	{
		// 0 <= rad <= 360 (or -360 <= rad <= 0)
		rad = fmod(rad, constants::two_pi<TReal>());
		// convert negative rad
		bool isNegative = signbit(rad);
		rad = fabs(rad);

		// 0 <= rad <= 90
		if(rad <= constants::half_pi<TReal>())
		{
			if(isNegative) return -sin(rad); else return sin(rad);
		}
		// 90 <= rad <= 180
		if(rad <= constants::pi<TReal>())
		{
			if(isNegative) return -sin(constants::pi<TReal>() - rad); else return sin(constants::pi<TReal>() - rad);
		}
		// 180 <= rad <= 270
		if(rad <= constants::pi<TReal>())
		{
			if(isNegative) return sin(rad - constants::pi<TReal>()); else return -sin(rad - constants::pi<TReal>());
		}
		// 270 <= rad <= 360
		if(isNegative) return sin(constants::two_pi<TReal>() - rad); else return -sin(constants::two_pi<TReal>() - rad);
	}

	template <class TReal>
	TReal calc_cos1(TReal rad)
	{
		return cos(rad);
	}

	template <class TReal>
	TReal calc_cos(TReal rad)
	{
		// 0 <= rad <= 360 (or -360 <= rad <= 0)
		rad = fmod(rad, constants::two_pi<TReal>());
		// convert negative rad
		if(rad < 0.0) rad = constants::two_pi<TReal>() + rad;

		// 0 <= rad <= 90
		if(rad <= constants::half_pi<TReal>())
		{
			return cos(rad);
		}
		// 90 <= rad <= 180
		if(rad <= constants::pi<TReal>())
		{
			return -cos(constants::pi<TReal>() - rad);
		}
		// 180 <= rad <= 270
		if(rad <= constants::pi<TReal>())
		{
			return -cos(rad - constants::pi<TReal>());
		}
		// 270 <= rad <= 360
		return cos(constants::two_pi<TReal>() - rad);
	}

	template <class TReal>
	TReal calc_cos2(TReal rad)
	{
		// 0 <= rad <= 360 (or -360 <= rad <= 0)
		rad = fmod(rad, constants::two_pi<TReal>());
		// convert negative rad
		rad = fabs(rad);

		// 0 <= rad <= 90
		if(rad <= constants::half_pi<TReal>())
		{
			return cos(rad);
		}
		// 90 <= rad <= 180
		if(rad <= constants::pi<TReal>())
		{
			return -cos(constants::pi<TReal>() - rad);
		}
		// 180 <= rad <= 270
		if(rad <= constants::pi<TReal>())
		{
			return -cos(rad - constants::pi<TReal>());
		}
		// 270 <= rad <= 360
		return cos(constants::two_pi<TReal>() - rad);
	}
}

#endif /* MATH_HPP */
