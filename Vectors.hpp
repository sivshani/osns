/*
 * based on the SFML vectors lib with some additions.
 */

#ifndef VECTORS_HPP
#define VECTORS_HPP

#include <iostream>

#include "boost/multiprecision/mpfr.hpp"
//#include "boost/multiprecision/float128.hpp"

#include <Math.hpp>

using namespace boost::multiprecision;
using namespace math;

namespace vec
{
	template <class T>
	struct Vector2;
	template <class T>
	struct Vector3;


	template <class T>
	struct Vector2
	{
		Vector2() : x(0), y(0){}
		Vector2(T x, T y) : x(x), y(y){}
		Vector2(const Vector2<T>& v2) = default;

		template <class U>
		explicit Vector2(const Vector2<U>& v2) :
		x(static_cast<T>(v2.x)),
		y(static_cast<T>(v2.y))
		{}

		T norm()
		{
			return sqrt(this->dotProduct());
		}

		Vector2<T> unitVector()
		{
			T norm = this->norm();
			return(Vector2<T>(this->x /norm, this->y /norm));
		}

		T dotProduct()
		{
			return(this->x * this->x + this->y * this->y);
		}

		T dotProduct(const Vector2<T>& rhs)
		{
			return(this->x * rhs.x + this->y * rhs.y);
		}

		Vector3<T> crossProduct()
		{
			return this->crossProduct(*this);
		}

		Vector3<T> crossProduct(const Vector2<T>& rhs)
		{
			Vector3<T> lhs(this->x, this->y, 0);
			return lhs.crossProduct(Vector3<T>(rhs.x, rhs.y, 0));;
		}

		T x;
		T y;
	};

	template <class T>
	Vector2<T> operator-(const Vector2<T>& rhs)
	{
		return Vector2<T>(-rhs.x, -rhs.y);
	}

	template <class T>
	Vector2<T> operator+(const Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		return Vector2<T>(lhs.x + rhs.x, lhs.y + rhs.y);
	}

	template <class T>
	Vector2<T>& operator+=(Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		lhs.x += rhs.x;
		lhs.y += rhs.y;

		return lhs;
	}

	template <class T>
	Vector2<T> operator-(const Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		return Vector2<T>(lhs.x - rhs.x, lhs.y - rhs.y);
	}

	template <class T>
	Vector2<T>& operator-=(Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		lhs.x -= rhs.x;
		lhs.y -= rhs.y;

		return lhs;
	}

	template <class T>
	Vector2<T> operator*(const Vector2<T>& lhs, T rhs)
	{
		return Vector2<T>(lhs.x * rhs, lhs.y * rhs);
	}

	template <class T>
	Vector2<T> operator*(T lhs, const Vector2<T>& rhs)
	{
		return rhs * lhs;
	}

	template <class T>
	Vector2<T>& operator*=(Vector2<T>& lhs, T rhs)
	{
		lhs.x *= rhs;
		lhs.y *= rhs;

		return lhs;
	}

	template <class T>
	Vector2<T> operator/(const Vector2<T>& lhs, T rhs)
	{
		return Vector2<T>(lhs.x / rhs, lhs.y / rhs);
	}

	template <class T>
	Vector2<T>& operator/=(Vector2<T>& lhs, T rhs)
	{
		lhs.x /= rhs;
		lhs.y /= rhs;

		return lhs;
	}

	template <class T>
	bool operator==(const Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		return (isApproxEqual(lhs.x, rhs.x) && isApproxEqual(lhs.y, rhs.y));
	}

	template <class T>
	bool operator!=(const Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		return !(lhs == rhs);
	}

	template <class T>
	std::ostream& operator<<(std::ostream& os, const Vector2<T>& rhs)
	{
		os << "(" << rhs.x << ", " << rhs.y << ")";
		return os;
	}

	typedef Vector2<int>          		Vector2i;
	typedef Vector2<unsigned int> 		Vector2u;
	typedef Vector2<float>        		Vector2f;
	typedef Vector2<double>       		Vector2d;
	typedef Vector2<mpfr_float_1000>	Vector2MPFR_f_1000;
//	typedef Vector2<float128>           Vector2f_128;
	//------------------------------------------------------------------------------

	template <class T>
	struct Vector3
	{
		Vector3() : x(0), y(0), z(0){};
		Vector3(T x, T y, T z) : x(x), y(y), z(z){};;
		Vector3(const Vector3<T>& v3) = default;

		template <class U>
		explicit Vector3(const Vector3<U>& v3) :
		x(static_cast<T>(v3.x)),
		y(static_cast<T>(v3.y)),
		z(static_cast<T>(v3.z))
		{}

		T norm()
		{
			return sqrt(this->dotProduct());
		}

		Vector3<T> unitVector()
		{
			T norm = this->norm();
			return(Vector3<T>(this->x / norm, this->y / norm, this->z / norm));
		}

		T dotProduct()
		{
			return(this->x * this->x + this->y * this->y + this->z * this->z);
		}

		T dotProduct(const Vector3<T>& rhs)
		{
			return(this->x * rhs.x + this->y * rhs.y + this->z * rhs.z);
		}

		Vector3<T> crossProduct()
		{
			return this->crossProduct(*this);

		}

		Vector3<T> crossProduct(const Vector3<T>& rhs)
		{
			return Vector3<T>(this->y * rhs.z - this->z * rhs.y,
							  this->z * rhs.x - this->x * rhs.z,
							  this->x * rhs.y - this->y * rhs.x);
		}

		T x;
		T y;
		T z;
	};

	template <class T>
	Vector3<T> operator-(const Vector3<T>& rhs)
	{
		return Vector3<T>(-rhs.x, -rhs.y, -rhs.z);
	}

	template <class T>
	Vector3<T> operator+(const Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		return Vector3<T>(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
	}

	template <class T>
	Vector3<T>& operator+=(Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		lhs.x += rhs.x;
		lhs.y += rhs.y;
		lhs.z += rhs.z;

		return lhs;
	}

	template <class T>
	Vector3<T> operator-(const Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		return Vector3<T>(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
	}

	template <class T>
	Vector3<T>& operator-=(Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		lhs.x -= rhs.x;
		lhs.y -= rhs.y;
		lhs.z -= rhs.z;

		return lhs;
	}

	template <class T>
	Vector3<T> operator*(const Vector3<T>& lhs, T rhs)
	{
		return Vector3<T>(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs);
	}

	template <class T>
	Vector3<T> operator*(T lhs, const Vector3<T>& rhs)
	{
		return rhs * lhs;
	}

	template <class T>
	Vector3<T>& operator*=(Vector3<T>& lhs, T rhs)
	{
		lhs.x *= rhs;
		lhs.y *= rhs;
		lhs.z *= rhs;

		return lhs;
	}

	template <class T>
	Vector3<T> operator/(const Vector3<T>& lhs, T rhs)
	{
		return Vector3<T>(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs);
	}

	template <class T>
	Vector3<T>& operator/=(Vector3<T>& lhs, T rhs)
	{
		lhs.x /= rhs;
		lhs.y /= rhs;
		lhs.z /= rhs;

		return lhs;
	}

	template <class T>
	bool operator==(const Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		return (isApproxEqual(lhs.x, rhs.x) && isApproxEqual(lhs.y, rhs.y) && isApproxEqual(lhs.z, rhs.z));
	}

	template <class T>
	bool operator!=(const Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		return !(lhs == rhs);
	}

	template <class T>
	std::ostream& operator<<(std::ostream& os, const Vector3<T>& rhs)
	{
		os << "(" << rhs.x << ", " << rhs.y << ", " << rhs.z << ")";
		return os;
	}

	typedef Vector3<int>          		Vector3i;
	typedef Vector3<unsigned int> 		Vector3u;
	typedef Vector3<float>        		Vector3f;
	typedef Vector3<double>       		Vector3d;
	typedef Vector3<mpfr_float_1000>	Vector3MPFR_f_1000;
//	typedef Vector3<float128>           Vector3f_128;
	//------------------------------------------------------------------------------
}

#endif //VECTORS_HPP
