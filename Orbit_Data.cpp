#include <Orbit_Data.hpp>

namespace Category
{
	std::string ST2string(SatType st)
	{
		if(st == SatType::GEO) return std::string("GEO");
		if(st == SatType::MEO) return std::string("MEO");
		if(st == SatType::LEO) return std::string("LEO");
		return std::string("GS");
	}

	std::string NT2string(NodeType nt)
	{
		if(NodeType::Member == nt) return std::string("Member");
		return std::string("Guest");
	}
}
