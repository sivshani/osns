#ifndef TEST_HPP
#define TEST_HPP

#include <vector>

typedef unsigned int uint;

// print functions
void midResults(uint test_number, bool status, std::vector<bool>& results);
void finalResults(const std::vector<bool>& results);


#endif /* TEST_HPP */
