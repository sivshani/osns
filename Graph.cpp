#include <Graph.hpp>

//#define ENABLE_DEBUG_C // comment out to disable debugging


//  myfile << "Writing this to a file.\n";
//  myfile.close();
void Graph::run(int chance)
{
	unsigned int interval = 1; // duration of interval in seconds
	unsigned int loop = 7200; // number of loops

	mpfr_float_1000 accTime = mpfr_float_1000(0.0);
	mpfr_float_1000 totalTime = mpfr_float_1000(0.0);

	Data data;

	// ***
	// init
	this->constellation.setConstellation();
//	this->constellation.debug();
	this->setVG();
//	this->debugVG();
	randLinkBreak(chance);
	this->setG();
//	this->printG();
	// ***

	for(unsigned int i = 0; i < loop; i++)
	{
//		std::cout << " Movement: " << std::endl;
		this->constellation.move(mpfr_float_1000(interval));
//		this->constellation.debug();
		// update time
		accTime += interval;
		totalTime += interval;

		this->setVG();
//		this->debugVG();
		randLinkBreak(chance);

		// *Old* graph agains new visibility!
//		// find and print lost links due to movement
		std::set<std::pair<unsigned int, unsigned int>> ll = lostLinks();
//		for(auto &e : ll)
//		{
//			std::cout << " Lost Links: " << e.first << ":" << e.second << std::endl;
//		}
		// break happen
		if(ll.size() > 0)
		{
			// check how many are not connected
			data.disconnected.push_back(notConnected());

			data.breaks++;
			data.intTimes.push_back(std::make_pair(accTime, maxPath()));

			accTime = 0;
		}
		this->setG();
//		this->printG();
	}
	data.allTimeInt = totalTime;
	data.calc();
//	data.print();
	data.write(chance);
}

int Graph::notConnected()
{
	int count = 0;
	for(auto &e : this->graph)
	{
		auto it = find_if(this->visibilityGraph.begin(), this->visibilityGraph.end(), [&](const visibilityVertex &obj) { return obj.id == e.id; });
		if((*it).nt_e == Category::Member)
		{
			if(e.adjl.size() < (*it).intf) count++;
		}
	}
	return count;
}

mpfr_float_1000 Graph::maxPath()
{
	mpfr_float_1000 d1 = mpfr_float_1000(0.0);
	mpfr_float_1000 d2 = mpfr_float_1000(0.0);

	for(auto &v1 : this->visibilityGraph)
	{
		if(v1.st_e == Category::GS)
		{
			for(auto &v2 : v1.visible)
			{
				if(v2.first.st_e == Category::GEO)
				{
					if(v2.second > d1)
					{
						d1 = v2.second;
					}
				}
			}
		}
		else if(v1.st_e == Category::GEO)
		{
			for(auto &v2 : v1.visible)
			{
				if(v2.first.st_e == Category::GEO)
				{
					if(v2.second > d2)
					{
						d2 = v2.second;
					}
				}
			}
		}
	}

	return d1 + d2;
}

std::set<std::pair<unsigned int, unsigned int>> Graph::lostLinks()
{
#ifdef ENABLE_DEBUG_C
std::cerr << "DEBUG " << __func__ << ":" << std::endl;
#endif

	// hold the IDs of all vertices that lost connectivity
	std::set<std::pair<unsigned int, unsigned int>> result;

	// check that each from the adjacency list is still in the visibility graph:
	for(auto &e : this->graph)
	{
		// get this vertex
		std::vector<visibilityVertex>::iterator this_v;
		for(this_v = this->visibilityGraph.begin(); this_v != this->visibilityGraph.end(); this_v++)
		{
			if((*this_v).id == e.id) break;
		}

#ifdef ENABLE_DEBUG_C
std::cerr << "e.id: " << e.id << "; (*this_v).id: " << (*this_v).id << std::endl;
#endif

		bool b = false;
		int count = 0;
		for(auto &el : e.adjl)
		{
			for(auto &ele : (*this_v).visible)
			{
#ifdef ENABLE_DEBUG_C
std::cerr << "    el: " << el << "; ele.first.id: " << ele.first.id << std::endl;
#endif
				if(el == ele.first.id)
				{
					b = true;
					break;
				}
			}
			if(!b) count++;
#ifdef ENABLE_DEBUG_C
std::cerr << "    b: " << b << "; count: " << count << std::endl;
#endif
			b = false;
		}

		if(count > 0)
		{
			result.insert(std::make_pair(e.id, count));
#ifdef ENABLE_DEBUG_C
std::cerr << "    e.id: " << e.id << "; count: " << count << std::endl;
#endif
		}
	}

	return result;
}

// populate the adjacency list and add to the graph.
void Graph::setG()
{
	/*
	 * for each element from the visibility graph:
	 * we need to populate x interfaces (as neighbors in the adjacency list)
	 * - for each interface, go over the visible neighbors:
	 *   - add visible neighbor as neighbor
	 *   - also, add this node to the neighbor as neighbor
	 *   - make sure both are in the graph
	 */
	this->graph.clear();

	// add know links
	for(auto &e : this->visibilityGraph)
	{
		Vertex v;
		v.id = e.id;

		// known links (GEO-GEO, GEO-GS)
		if(1 == e.id)
		{
			v.adjl.push_back(6);
			v.adjl.push_back(2);
			v.adjl.push_back(7);
			v.adjl.push_back(8);
		}
		if(2 == e.id)
		{
			v.adjl.push_back(1);
			v.adjl.push_back(3);
			v.adjl.push_back(9);
			v.adjl.push_back(10);
		}
		if(3 == e.id)
		{
			v.adjl.push_back(2);
			v.adjl.push_back(4);
			v.adjl.push_back(11);
			v.adjl.push_back(12);
		}
		if(4 == e.id)
		{
			v.adjl.push_back(3);
			v.adjl.push_back(5);
			v.adjl.push_back(13);
			v.adjl.push_back(14);
		}
		if(5 == e.id)
		{
			v.adjl.push_back(4);
			v.adjl.push_back(6);
			v.adjl.push_back(15);
			v.adjl.push_back(16);
		}
		if(6 == e.id)
		{
			v.adjl.push_back(5);
			v.adjl.push_back(1);
			v.adjl.push_back(17);
			v.adjl.push_back(18);
		}
		if(7 == e.id)
		{
			v.adjl.push_back(1);
		}
		if(8 == e.id)
		{
			v.adjl.push_back(1);
		}
		if(9 == e.id)
		{
			v.adjl.push_back(2);
		}
		if(10 == e.id)
		{
			v.adjl.push_back(2);
		}
		if(11 == e.id)
		{
			v.adjl.push_back(3);
		}
		if(12 == e.id)
		{
			v.adjl.push_back(3);
		}
		if(13 == e.id)
		{
			v.adjl.push_back(4);
		}
		if(14 == e.id)
		{
			v.adjl.push_back(4);
		}
		if(15 == e.id)
		{
			v.adjl.push_back(5);
		}
		if(16 == e.id)
		{
			v.adjl.push_back(5);
		}
		if(17 == e.id)
		{
			v.adjl.push_back(6);
		}
		if(18 == e.id)
		{
			v.adjl.push_back(6);
		}

		this->graph.push_back(v);
	}

	for(auto &e : this->visibilityGraph)
	{
		std::vector<Vertex>::iterator this_v, neig_v;
		// get this vertex
		for(this_v = this->graph.begin(); this_v != this->graph.end(); this_v++)
		{
			if((*this_v).id == e.id) break;
		}

		// find visible neighbor that is not in the adjacency list:
		for(auto &el : e.visible)
		{
			// get the neighbor vertex
			for(neig_v = this->graph.begin(); neig_v != this->graph.end(); neig_v++)
			{
				if((*neig_v).id == el.first.id) break;
			}

			// check that the neighbor has free interface and this vertex is not already in it's adjacency list, and add it
			if(((*neig_v).adjl.empty() || ((*neig_v).adjl.size() < el.first.intf)) &&
				((*this_v).adjl.empty() || std::find((*this_v).adjl.begin(), (*this_v).adjl.end(), el.first.id) == (*this_v).adjl.end()))
			{
				(*this_v).adjl.push_back(el.first.id);

				// now add this vertex to the neighbor
				if((*neig_v).adjl.empty() || std::find((*neig_v).adjl.begin(), (*neig_v).adjl.end(), el.first.id) == (*neig_v).adjl.end()) (*neig_v).adjl.push_back(e.id);
			}

			// if interfaces are full - break
			if((*this_v).adjl.size() == e.intf) break;
		}
	}

	// sort by id
	std::sort(this->graph.begin(), this->graph.end(),
			  [&](const Vertex& obg1, const Vertex& obg2){ return (obg1.id < obg2.id); });
}

// auto it = find_if(vec.begin(), vec.end(), [&id](const std::pair<visibilityVertex, mpfr_float_1000>& obj) { return (obj).first.id == id; });
void Graph::randLinkBreak(int chance)
{
	if(0 == chance) return;

	// random chance of link breaking
	std::vector<visibilityVertex>::iterator v1;
	for(v1 = this->visibilityGraph.begin(); v1 != this->visibilityGraph.end(); v1++)
	{
		std::vector<std::pair<visibilityVertex, mpfr_float_1000>>::iterator e;
		for(e = (*v1).visible.begin(); e != (*v1).visible.end();)
		{
			int r(std::rand() % chance);
			if(r < 1) // since rand() includes 0
			{
				// remove here
				(*v1).visible.erase(e);
				// remove at the removed element visibility list
				// the other element
				auto it1 = find_if(this->visibilityGraph.begin(), this->visibilityGraph.end(), [&](const visibilityVertex &obj) { return obj.id == (*e).first.id; });
				// this element from the other element visibility list
				auto it2 = find_if((*it1).visible.begin(), (*it1).visible.end(), [&](const std::pair<visibilityVertex, mpfr_float_1000>  &obj) { return obj.first.id == (*v1).id; });
				(*it1).visible.erase(it2);
			}
			else
			{
				e++;
			}
		}

	}
}

void Graph::setVG()
{
	this->visibilityGraph.clear();

	for(auto &e : this->constellation.nodes)
	{
		visibilityVertex v;
		v.id   = e.getID();
		v.st_e = e.getSatType_e();
		v.st_s = e.getSatType_s();
		v.nt_e = e.getNodeType_e();
		v.nt_s = e.getNodeType_s();
		v.sl   = e.getSL();

		mpfr_float_1000 d(0.0);
		bool b = false;
		for(auto &el : this->constellation.nodes)
		{
			// with itself
			if(el.getID() == e.getID()) continue;
			// already tested
			if(v.doesVertexExists(el.getID(), v.visible) || v.doesVertexExists(el.getID(), v.not_visible)) continue;
			// guest-guest
			if(Category::NodeType::Guest == e.getNodeType_e() && Category::NodeType::Guest == el.getNodeType_e()) continue;
			// guest - GS
			if((Category::NodeType::Guest == e.getNodeType_e() && Category::SatType::GS == el.getSatType_e()) ||
			   (Category::NodeType::Guest == el.getNodeType_e() && Category::SatType::GS == e.getSatType_e())) continue;

			visibilityVertex v1;
			v1.id   = el.getID();
			v1.st_e = el.getSatType_e();
			v1.st_s = el.getSatType_s();
			v1.nt_e = el.getNodeType_e();
			v1.nt_s = el.getNodeType_s();
			v1.sl   = el.getSL();

			// GS with GS
			if(Category::SatType::GS == e.getSatType_e() && Category::SatType::GS == el.getSatType_e())
			{
				b = true;
				d = Misc::distance_GS_GS;
			}

			// GS with GEO
			else if((Category::SatType::GEO == e.getSatType_e() && Category::SatType::GS == el.getSatType_e()) ||
					(Category::SatType::GEO == el.getSatType_e() && Category::SatType::GS == e.getSatType_e()))
			{
				constellation.areVisible(e, el, d);

				if(Category::SatType::GEO == e.getSatType_e() && Category::SatType::GS == el.getSatType_e())
				{
					if((1 == e.getID() && (7  == el.getID() || 8  == el.getID())) ||
					   (2 == e.getID() && (9  == el.getID() || 10 == el.getID())) ||
					   (3 == e.getID() && (11 == el.getID() || 12 == el.getID())) ||
					   (4 == e.getID() && (13 == el.getID() || 14 == el.getID())) ||
					   (5 == e.getID() && (15 == el.getID() || 16 == el.getID())) ||
					   (6 == e.getID() && (17 == el.getID() || 18 == el.getID())))
					{
						b = true;
					}
				}
				else if(Category::SatType::GEO == el.getSatType_e() && Category::SatType::GS == e.getSatType_e())
				{
					if((1 == el.getID() && (7  == e.getID() || 8  == e.getID())) ||
					   (2 == el.getID() && (9  == e.getID() || 10 == e.getID())) ||
					   (3 == el.getID() && (11 == e.getID() || 12 == e.getID())) ||
					   (4 == el.getID() && (13 == e.getID() || 14 == e.getID())) ||
					   (5 == el.getID() && (15 == e.getID() || 16 == e.getID())) ||
					   (6 == el.getID() && (17 == e.getID() || 18 == e.getID())))
					{
						b = true;
					}
				}
				else
				{
					b = false;
				}
			}

			// the general case
			else
			{
				b = constellation.areVisible(e, el, d);
			}

			if(b) v.visible.push_back(std::make_pair(v1, d));
			else v.not_visible.push_back(std::make_pair(v1, d));

			b = false;
			d = mpfr_float_1000(0.0);
		}

		v.setIntf();
		v.setPriority();
		this->visibilityGraph.push_back(v);
	}
	// sort by priority and type
	std::sort(this->visibilityGraph.begin(), this->visibilityGraph.end(),
			  [&](const visibilityVertex& obg1, const visibilityVertex& obg2){ return (obg1.st_e > obg2.st_e) || ((obg1.st_e == obg2.st_e) && (obg1.priority < obg2.priority)); });
}

void Graph::printG()
{
	std::cout << "PRINT G" << std::endl;
	for(auto &e : this->graph)
	{
		std::cout << e.id << ": ";
		for(auto &el : e.adjl)
		{

			std::cout << el << "; ";
		}
		std::cout << std::endl;
	}
}

void Graph::printVG()
{
	std::cout << "PRINT VG" << std::endl;
	for(auto &e : this->visibilityGraph)
	{
		std::cout << "(" << e.st_s << " " << e.id << " " << e.nt_s << ", " << rad2Deg(e.sl.Equat.y) << ", " << rad2Deg(e.sl.Equat.z) << "):" << std::endl;

		std::cout << "visible:" << std::endl;
		for(auto &el : e.visible)
		{
			std::cout << "(" << el.first.st_s << " " << el.first.id << " " << el.first.nt_s << ", " << rad2Deg(el.first.sl.Equat.y) << ", " << rad2Deg(el.first.sl.Equat.z) << "), " << el.second << ";" << std::endl;
		}
		std::cout << "not visible:" << std::endl;
		for(auto &el : e.not_visible)
		{
			std::cout << "(" << el.first.st_s << " " << el.first.id << " " << el.first.nt_s << ", " << rad2Deg(el.first.sl.Equat.y) << ", " << rad2Deg(el.first.sl.Equat.z) << "), " << el.second << ";" << std::endl;
		}
		std::cout << std::endl;
	}
}

void Graph::debugVG()
{
	std::cout << "DEBUG VG" << std::endl;
	for(auto &e : this->visibilityGraph)
	{
		std::cout << e.id << ": ";

		for(auto &el : e.visible)
		{
			std::cout << el.first.id << "; ";
		}
		std::cout << std::endl;
	}
}
