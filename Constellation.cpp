#include <Constellation.hpp>
#include <Math.hpp>

//#define ENABLE_DEBUG // comment out to disable debugging


void Constellation::printNodes()
{
	std::cout << "Constellation:" << std::endl;

	for(auto &e : this->nodes)
	{
		std::cout << "SAT " << e.getID() << ", " << e.getSatType_s() << ", " << e.getNodeType_s() << ": " << std::endl;
		e.print();
		std::cout << std::endl;
	}
}

void Constellation::debug()
{
	std::cout << "Nodes:" << std::endl;

	for(auto &e : this->nodes)
	{
		std::cout << "SAT " << e.getID() << ", " << e.getNodeType_s() << ": " << std::endl;
		std::cout << "(" << e.getSatType_s() << ", " << rad2Deg(e.getSL().Equat.y) << ", " << rad2Deg(e.getSL().Equat.z) << ")" << std::endl;
	}
}

// move, takes seconds
void Constellation::move(mpfr_float_1000 seconds)
{
	for(auto &e : nodes)
	{
		e.move(seconds);
	}
}

/*
 * determines if 2 satellites are visible to each other
 * if yes, return the distance btween them (in meters)
 * if not, return 0.
 */
bool Constellation::areVisible(Node<mpfr_float_1000> n1, Node<mpfr_float_1000> n2, mpfr_float_1000 &d)
{
#ifdef ENABLE_DEBUG
std::cerr << "DEBUG " << __func__ << ":" << std::endl;
std::cerr << "n1: ";
n1.debug();
std::cerr << "n2: ";
n2.debug();
#endif

	// 2 conditions has to be true: boolean true and distance > 0
	bool cond1, cond2 = false;
	mpfr_float_1000 distance = mpfr_float_1000(0.0);

	// 2 GSs are always linked (Internet)
	if(Category::SatType::GS == n1.getSatType_e() && Category::SatType::GS == n2.getSatType_e())
	{
		distance = Misc::distance_GS_GS;
		cond1 = true;
		cond2 = true;
	}
	// GS and non-GEO are never linked
	else if((Category::SatType::GS == n1.getSatType_e() && Category::SatType::GEO != n2.getSatType_e()) ||
			(Category::SatType::GS == n2.getSatType_e() && Category::SatType::GEO != n1.getSatType_e()))
	{
		areInRange(n1.getCL().XYZ, n2.getCL().XYZ, distance);
		cond1 = false;
		cond2 = false;
	}
	// SAT - SAT: need to be in range and ray not through atmosphere
	else if(Category::SatType::GS != n1.getSatType_e() && Category::SatType::GS != n2.getSatType_e())
	{
		cond1 = areInRange(n1.getCL().XYZ, n2.getCL().XYZ, distance);
		cond2 = !raySphere(n1.getCL().XYZ, n2.getCL().XYZ); // raySphere return false if they do not intersect!
	}
	// 2 GSs or GS and not GEO: not relevant
	else if((Category::SatType::GS == n1.getSatType_e() && Category::SatType::GEO == n2.getSatType_e()) ||
			(Category::SatType::GS == n2.getSatType_e() && Category::SatType::GEO == n1.getSatType_e()))
	{
		cond1 = areInRange(n1.getCL().XYZ, n2.getCL().XYZ, distance);

		if(Category::SatType::GEO == n1.getSatType_e())
		{
			cond2 = isInCone(n1, n2);
		}
		else if(Category::SatType::GEO == n2.getSatType_e())
		{
			cond2 = isInCone(n2, n1);
		}
	}
	else
	{
		std::cout << "Error " << __func__ << ": not SAT and not GS?" << std::endl;
		return 0;
	}

	d = distance;
	if(cond1 && cond2) return true;
	else return false;
}

// determines if 2 satellites are in range
bool Constellation::areInRange(vec::Vector3MPFR_f_1000 p1, vec::Vector3MPFR_f_1000 p2, mpfr_float_1000 &d)
{
#ifdef ENABLE_DEBUG
std::cerr << "	DEBUG " << __func__ << ":" << std::endl;
std::cerr << "	p1: "<< p1 << std::endl;
std::cerr << "	p2: "<< p2 << std::endl;
#endif

	d = (p2 - p1).norm();

#ifdef ENABLE_DEBUG
std::cerr << "	d: "<< d << std::endl;
#endif

	if (d <= Misc::range) return true;
	else return false;
}

/*
 * determines if line intersect with a sphere.
 * take 2 3d points (line start and end)and sphere center and radius
 * false: no intersection
 * true: intersect
 * source:
 * http://paulbourke.net/geometry/circlesphere/raysphere.c
 * http://www.ambrsoft.com/TrigoCalc/Sphere/SpherLineIntersection_.htm
 */
bool Constellation::raySphere(vec::Vector3MPFR_f_1000 p1, vec::Vector3MPFR_f_1000 p2, vec::Vector3d sc, mpfr_float_1000 r)
{
#ifdef ENABLE_DEBUG
std::cerr << "	DEBUG " << __func__ << ":" << std::endl;
std::cerr << "	p1: "<< p1 << std::endl;
std::cerr << "	p2: "<< p2 << std::endl;
#endif

	mpfr_float_1000 a, b, c;	// quadratic equation
	mpfr_float_1000 bb4ac;		// quadratic equation
	vec::Vector3MPFR_f_1000 dp; // the line

   dp.x = p2.x - p1.x;
   dp.y = p2.y - p1.y;
   dp.z = p2.z - p1.z;
   a = dp.x * dp.x + dp.y * dp.y + dp.z * dp.z;
   b = 2 * (dp.x * (p1.x - sc.x) + dp.y * (p1.y - sc.y) + dp.z * (p1.z - sc.z));
   c = sc.x * sc.x + sc.y * sc.y + sc.z * sc.z;
   c += p1.x * p1.x + p1.y * p1.y + p1.z * p1.z;
   c -= 2 * (sc.x * p1.x + sc.y * p1.y + sc.z * p1.z);
   c -= r * r;
   bb4ac = b * b - 4 * a * c;

#ifdef ENABLE_DEBUG
std::cerr << "	bb4ac: " << bb4ac << std::endl;
std::cerr << "	a: " << a << std::endl;
#endif

	if (a < std::numeric_limits<mpfr_float_1000>::epsilon() || bb4ac < 0) return false;
	return true;
}

/*
 * determine if a GS is in the cone of a GEO
 * https://stackoverflow.com/questions/12826117/how-can-i-detect-if-a-point-is-inside-a-cone-or-not-in-3d-space
 */
bool Constellation::isInCone(Node<mpfr_float_1000> GEO, Node<mpfr_float_1000> GS)
{
#ifdef ENABLE_DEBUG
std::cerr << "	DEBUG " << __func__ << ":" << std::endl;
std::cerr << "	n1: ";
GEO.debug();
std::cerr << "	n2: ";
GS.debug();
#endif

	// the cone's tip
	vec::Vector3MPFR_f_1000 x = GEO.getCL().XYZ;

	// normalized axis vector (x -> earth center)
	vec::Vector3MPFR_f_1000 dir(-x.x, -x.y, -x.z);
	mpfr_float_1000 norm = sqrt(x.x * x.x + x.y * x.y + x.z * x.z);
	dir.x = dir.x / norm;
	dir.y = dir.y / norm;
	dir.z = dir.z / norm;

	// height
	mpfr_float_1000 h = GEO.getOrbit().radius;

	// base radius
	mpfr_float_1000 hyp = h / calc_cos(Misc::angleR);
	mpfr_float_1000 r = hyp * calc_sin(Misc::angleR);

	// point to test
	vec::Vector3MPFR_f_1000 p = GS.getCL().XYZ;

	// project p onto dir
	mpfr_float_1000 cone_dist = (p - x).dotProduct(dir);

#ifdef ENABLE_DEBUG
std::cerr << "	cone_dist: " << cone_dist << "\n";
#endif

	// if not 0 <= cone_dist <= h return false
	if(cone_dist < 0 || cone_dist > h) return false;

	// cone radius at this point:
	mpfr_float_1000 cone_radius = (cone_dist / h) * r;

	// orthogonal distance from the axis
	mpfr_float_1000 orth_distance = ((p - x) - (dir * cone_dist)).norm();

	if(cone_radius < orth_distance) return false;

#ifdef ENABLE_DEBUG
std::cerr << "	cone_radius: " << cone_radius << "\n";
#endif

	return true;
}

void Constellation::setConstellation()
{
	// GEOs
	KOE_MPFR_f_1000 koe_GEO_1(mpfr_float_1000(Satellites::GEO::radius), mpfr_float_1000(0.0), mpfr_float_1000(0.0), mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GEO_1 (koe_GEO_1, 1, Category::Member);
	this->nodes.push_back(node_GEO_1);

	KOE_MPFR_f_1000 koe_GEO_2(mpfr_float_1000(Satellites::GEO::radius), mpfr_float_1000(0.0), RAD_60_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GEO_2 (koe_GEO_2, 2, Category::Member);
	this->nodes.push_back(node_GEO_2);

	KOE_MPFR_f_1000 koe_GEO_3(mpfr_float_1000(Satellites::GEO::radius), mpfr_float_1000(0.0), RAD_120_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GEO_3 (koe_GEO_3, 3, Category::Member);
	this->nodes.push_back(node_GEO_3);

	KOE_MPFR_f_1000 koe_GEO_4(mpfr_float_1000(Satellites::GEO::radius), mpfr_float_1000(0.0), RAD_180_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GEO_4 (koe_GEO_4, 4, Category::Member);
	this->nodes.push_back(node_GEO_4);

	KOE_MPFR_f_1000 koe_GEO_5(mpfr_float_1000(Satellites::GEO::radius), mpfr_float_1000(0.0), RAD_240_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GEO_5 (koe_GEO_5, 5, Category::Member);
	this->nodes.push_back(node_GEO_5);

	KOE_MPFR_f_1000 koe_GEO_6(mpfr_float_1000(Satellites::GEO::radius), mpfr_float_1000(0.0), RAD_300_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GEO_6 (koe_GEO_6, 6, Category::Member);
	this->nodes.push_back(node_GEO_6);

	// GSs, spaced out on the equator, 15 degrees east and west of their respective DEO.
	KOE_MPFR_f_1000 koe_GS_1(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_345_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_1 (koe_GS_1, 7, Category::Member);
	this->nodes.push_back(node_GS_1);

	KOE_MPFR_f_1000 koe_GS_2(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_15_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_2 (koe_GS_2, 8, Category::Member);
	this->nodes.push_back(node_GS_2);

	KOE_MPFR_f_1000 koe_GS_3(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_45_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_3 (koe_GS_3, 9, Category::Member);
	this->nodes.push_back(node_GS_3);

	KOE_MPFR_f_1000 koe_GS_4(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_75_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_4 (koe_GS_4, 10, Category::Member);
	this->nodes.push_back(node_GS_4);

	KOE_MPFR_f_1000 koe_GS_5(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_105_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_5 (koe_GS_5, 11, Category::Member);
	this->nodes.push_back(node_GS_5);

	KOE_MPFR_f_1000 koe_GS_6(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_135_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_6 (koe_GS_6, 12, Category::Member);
	this->nodes.push_back(node_GS_6);

	KOE_MPFR_f_1000 koe_GS_7(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_165_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_7 (koe_GS_7, 13, Category::Member);
	this->nodes.push_back(node_GS_7);

	KOE_MPFR_f_1000 koe_GS_8(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_195_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_8 (koe_GS_8, 14, Category::Member);
	this->nodes.push_back(node_GS_8);

	KOE_MPFR_f_1000 koe_GS_9(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_225_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_9 (koe_GS_9, 15, Category::Member);
	this->nodes.push_back(node_GS_9);

	KOE_MPFR_f_1000 koe_GS_10(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_255_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_10 (koe_GS_10, 16, Category::Member);
	this->nodes.push_back(node_GS_10);

	KOE_MPFR_f_1000 koe_GS_11(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_285_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_11 (koe_GS_11, 17, Category::Member);
	this->nodes.push_back(node_GS_11);

	KOE_MPFR_f_1000 koe_GS_12(mpfr_float_1000(Earth::radius), mpfr_float_1000(0.0), RAD_315_M, mpfr_float_1000(0.0));
	Node<mpfr_float_1000> node_GS_12 (koe_GS_12, 18, Category::Member);
	this->nodes.push_back(node_GS_12);


	// for generating random number
	std::random_device rd;
	std::mt19937 gen(rd());

	// for the inclination
	std::uniform_real_distribution<> inc_dis(0.0, RAD_60_D);
	// for the longitude
	std::uniform_real_distribution<> lon_dis(0.0, RAD_360_D);
	// for the mean anomaly
	std::uniform_real_distribution<> mea_dis(0.0, RAD_360_D);


	// generates 24 random satellites
	unsigned int id = 19;
	std::srand(std::time(0));
	for(int i = 0; i < 24; i++)
	{
		// to decide MEO/LEO
		mpfr_float_1000 r(std::rand() % 2 ? Satellites::MEO::radius : Satellites::LEO::alt_max + Earth::radius);
		// get inclination
		mpfr_float_1000 inc(inc_dis(rd));
		// get longitude
		mpfr_float_1000 lon(lon_dis(rd));
		// get  mean anomaly
		mpfr_float_1000 mea(mea_dis(rd));

		KOE_MPFR_f_1000 koe(r, inc, lon, mea);

		Node<mpfr_float_1000> node(koe, id);

		this->nodes.push_back(node);
		id++;
	}
}
