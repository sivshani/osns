#ifndef ORBIT_DATA_HPP
#define ORBIT_DATA_HPP

#include <cmath>
#include <string>

#include "boost/multiprecision/mpfr.hpp"

#include <Orbit.hpp>

using namespace boost::math;
using namespace boost::multiprecision;


namespace Category
{
	enum SatType
	{
		GS  = 0,
		GEO = 1 << 0,
		MEO = 1 << 1,
		LEO = 1 << 2,
	};

	std::string ST2string(SatType st);

	enum NodeType
	{
		None    = 0,
		Member	= 1,
		Guest	= 2,
	};

	std::string NT2string(NodeType nt);
}


// earth
namespace Earth
{
	const SphericalLocation<double> earthCenterSpherical; // [0, 0, 0]
	const CartesianLocation<double> earthCenterCartesian; // [0, 0, 0]

	// Radius: in meters, according to IAU's nominal "zero tide" polar/equatorial radius.
	const double radiusPolar              = 6356800; // meters
	const double radiusEquatorial         = 6378100; // meters
	const double radius                   = (radiusPolar + radiusEquatorial)/2.0; // meters, 6367450
	const double circumferencePolar       = M_PI * 2.0 * radiusPolar; // meters
	const double circumferenceEquatorial  = M_PI * 2.0 * radiusEquatorial; // meters
	const double circumference            = M_PI * 2.0 * radius; // meters
	const double kramanLine				  = 100000; // meters
	const double radiusAthmosphere		  = radius + kramanLine; // meters
	const double meterPerRadianPolar      = radiusPolar; // circumferencePolar/(2 * M_PI);
	const double meterPerRadianEquatorial = radiusEquatorial; // circumferenceEquatorial/(2 * M_PI);
	const double meterPerRadian           = radius; // circumferenceEquatorial/(2 * M_PI);
	const double meterPerDegreePolar      = circumferencePolar/360.0;
	const double meterPerDegreeEquatorial = circumferenceEquatorial/360.0;
	const double meterPerDegree           = circumference/360.0;
	const double period                   = 0; // seconds
	const double velocity                 = circumference/period; // seconds

	//Speed of rotation on earth toward east
	inline double speedOfPointOnEarth(double declinationAngle){ return math::calc_cos(declinationAngle) * velocity; };

	// mass
	const double mass                     = 5.972e24; // Kg
}

// physics
namespace Physics
{
	const double speedOfLight          = 2.99792458e8; // meters per second
	const double gravitationalConstant = 6.67430e-11;  // N m2/kg2
	// takes a cartesian location in orbit around earth and return velocity
	template <class TReal>
	inline TReal velocity(TReal r){ return sqrt(Physics::gravitationalConstant * Earth::mass / r); }; // meters per second
	template <class TReal>
	inline TReal velocity(vec::Vector3<TReal> cl){ return sqrt(Physics::gravitationalConstant * Earth::mass / sqrt(cl.x*cl.x + cl.y*cl.y + cl.z*cl.z)); }; // meters per second
}

// orbit data
namespace Satellites
{
	namespace GEO
	{
		const double altitude 		= 35786000;                 	// meters
		const double radius 		= altitude + Earth::radius; 	// meters, 35786000 + 6367450 = 42153450
		const double circumference 	= M_PI * 2.0 * radius;      	// meters
		const double period 		= 0;                        	// seconds
	}

	namespace MEO
	{
		const double alt_min        = 2000000;						// meters, 8367450
		const double alt_max        = 35786000;						// meters, 42153450
		const double altitude 		= 7500000;						// meters
		const double radius 		= altitude + Earth::radius; 	// meters
		const double circumference 	= M_PI * 2.0 * radius;      	// meters
		const double velocity 	    = Physics::velocity(radius);	// meters/second
		const double period 		= circumference / velocity;     // seconds
	}

	namespace LEO
	{
		const double alt_min        = 161000;                   	// meters, 6528450
		const double alt_max        = 2000000;                  	// meters, 8367450
		const double altitude 		= 450000;                   	// meters
		const double radius 		= altitude + Earth::radius; 	// meters
		const double circumference 	= M_PI * 2.0 * radius;      	// meters
		const double velocity 	    = Physics::velocity(radius);	// meters/second
		const double period 		= circumference / velocity;     // seconds
	}
}

namespace Misc
{
	const double range = 50000000; // meters, set optical communication range to 40,000 km
	const double angleD = 40.0;     // set the optical communication angle in degrees (relevant for GEO ground cover)
	const double angleR = deg2Rad(angleD);
	const double distance_GS_GS = 60000000; // meters, the distance between 2 GS on earth defined as the distance that time cross in 200 ms (upper lag time on the Internet)
	const double distance_GEO_GS = 42000000; // meters, the distance between 2 GS on earth defined as the distance that time cross in 200 ms (upper lag time on the Internet)
}

#endif /* ORBIT_DATA_HPP */
