#ifndef ORBIT_HPP
#define ORBIT_HPP

#include <boost/multiprecision/mpfr.hpp>

#include <Vectors.hpp>
#include <Math.hpp>

using namespace boost::math;
using namespace boost::multiprecision;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Spherical location
// Spherical coordinates are the natural curvilinear coordinates system for describing positions on a sphere or spheroid.
// This is a variant of the Equatorial Coordinate that measures the right ascension as [VE->East : 0-Pi; VE->West : 0-(-PI)] and not [VE->East : 0-2Pi]
//
// x: the radius from the earth's center
// y: the right ascension, azimuth from the vernal equinox, in radians (VE->East : 0-2Pi)
// z: the declination, angle from the equator in radians (Eq->North : 0-Pi/2; Eq->South : 0-(-Pi/2))
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <class TReal>
struct SphericalLocation
{
public:
    SphericalLocation() = default;
    SphericalLocation(TReal Radius, TReal RA, TReal D)
    : Equat(Radius, RA, D)
    {
    }
    SphericalLocation(vec::Vector3<TReal> sl)
    : Equat(sl)
    {
    }

    vec::Vector3<TReal> Equat; // radius, right ascension, declination
    void set(TReal x, TReal y, TReal z)
    {
    	this->Equat.x = x;
    	this->Equat.y = y;
    	this->Equat.z = z;
    }
};

template <class TReal>
bool operator==(const SphericalLocation<TReal>& lhs, const SphericalLocation<TReal>& rhs)
{
	return (isApproxEqual(lhs.Equat.x, rhs.Equat.x) && isApproxEqual(rad2Deg(lhs.Equat.y), rad2Deg(rhs.Equat.y)) && isApproxEqual(rad2Deg(lhs.Equat.z), rad2Deg(rhs.Equat.z)));
}

template <class TReal>
std::ostream& operator<<(std::ostream& os, const SphericalLocation<TReal>& rhs)
{
	os << "(" << rhs.Equat.x << ", " << rad2Deg(rhs.Equat.y) << ", " << rad2Deg(rhs.Equat.z) << ")";
	return os;
}

typedef SphericalLocation<mpfr_float_1000>	Sl_MPFR_f_1000;
//typedef SphericalLocation<float128>         Sl_f_128;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Cartesian location
// Cartesian coordinates system.
// This is a variant of the ECEF Coordinate system, but earth is simplified to a perfect sphere.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <class TReal>
struct CartesianLocation
{
public:
    CartesianLocation() = default;
    CartesianLocation(TReal X, TReal Y, TReal Z):
    XYZ(X, Y, Z)
    {
    }
    CartesianLocation (vec::Vector3<TReal> cl)
    : XYZ(cl)
    {
    }

    vec::Vector3<TReal> XYZ;
    void set(TReal x, TReal y, TReal z)
    {
    	this->XYZ.x = x;
    	this->XYZ.y = y;
    	this->XYZ.z = z;
    }
};

template <class TReal>
std::ostream& operator<<(std::ostream& os, const CartesianLocation<TReal>& rhs)
{
	os << rhs.XYZ;
	return os;
}

typedef CartesianLocation<mpfr_float_1000>	Cl_MPFR_f_1000;
//typedef CartesianLocation<float128>         Cl_f_128;


// conversion functions
template <class TReal>
vec::Vector3<TReal> spherical2Cartesian(vec::Vector3<TReal> sl)
{
	TReal x = sl.x * math::calc_cos(sl.y) * math::calc_cos(sl.z);
    TReal y = sl.x * math::calc_sin(sl.y) * math::calc_cos(sl.z);
    TReal z = sl.x * math::calc_sin(sl.z);

    return vec::Vector3<TReal>(x, y, z);
}

template <class TReal>
vec::Vector3<TReal> cartesian2Spherical(vec::Vector3<TReal> cl)
{
	TReal r = sqrt(cl.x*cl.x + cl.y*cl.y + cl.z*cl.z);
	TReal ra = atan2(cl.y, cl.x);
	if(ra < 0.0) ra = constants::two_pi<TReal>() + ra;
	TReal d = asin(cl.z/r);

    return vec::Vector3<TReal>(r, ra, d);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Keplerian Orbit Elements
// Define an orbit over a perfect sphere
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <class TReal>
struct KeplerianOrbitElements
{
public:
	// variables
	// the semi-major axis is the maximal radius of the satellite's orbit from earth's center.
	// since the orbit is circular, semi-major axis = semi-minor axis = radius.
	TReal radius{}; // meters

	// eccentricity = 0 -> circle
	// eccentricity >= 1 -> parabola/hyperbola
	TReal eccentricity = 0.0; // "e", 0 <= eccentricity < 1

	// the angle between the plane of the orbit and the equator.
	// 0 - PI/2: prograde orbit
	// PI/2 - PI: retrograde orbit
	TReal inclination{}; // "i", radians

	// longitude of the ascending node:
	// the angle between the reference direction and where the orbit crosses from below the reference plane to above it.
	// - the intersection between the orbital plane and the reference plane.
	// 0 - 2PI, counter clockwise
	TReal longitude_of_AN{}; // "Ω", radians

	// the angle between the ascending node and periapsis (the lowest point in the orbit)
	// since the orbit is circular, the periapsis is undefined
	// 0 - PI/2
	TReal argument_of_periapsis = -1.0; // "ω", radians

	// the position of the body in orbit as a degree from the ascending node
	// 0-2PI
	TReal mean_anomaly{}; // "M", radians

	// derivatives
	TReal circumference{}; // meters

	KeplerianOrbitElements() = default;
	// radius, inclination, longitude of ascending node
	KeplerianOrbitElements(TReal r, TReal i, TReal l, TReal m)
	{
		if(r < 0.0)
		{
			throw std::invalid_argument( "radius < 0" );
		}
		this->radius = r;

		if(i < 0.0 || i > constants::pi<TReal>())
		{
			throw std::invalid_argument( "inclination < 0 || inclination > PI" );
		}
		this->inclination = i;

		if(l < 0.0 || l > constants::two_pi<TReal>())
		{
			throw std::invalid_argument( "longitude_of_AN < 0 || longitude_of_AN > PI * 2" );
		}
		this->longitude_of_AN = l;

		if(m < 0.0 || m > constants::two_pi<TReal>())
		{
			throw std::invalid_argument( "mean_anomaly < 0 || mean_anomaly > PI * 2" );
		}
		this->mean_anomaly = m;

		// derivatives
		circumference = constants::two_pi<TReal>() * radius;
	}

    void set(TReal r, TReal i, TReal l, TReal m)
    {
    	this->radius = r;
    	this->inclination = i;
    	this->longitude_of_AN = l;
    	this->mean_anomaly = m;
    }
};

typedef KeplerianOrbitElements<mpfr_float_1000>	 KOE_MPFR_f_1000;
//typedef KeplerianOrbitElements<float128>         KOE_f_128;

template <class TReal>
std::ostream& operator<<(std::ostream& os, const KeplerianOrbitElements<TReal>& rhs)
{
    os << "Keplerian Orbit Elements:" << std::endl
	   << "   Radius: " << rhs.radius << std::endl
//	   << "   Eccentricity: " << rhs.eccentricity << std::endl
	   << "   Inclination: " << rhs.inclination << std::endl
	   << "   Longitude of ascending node: " << rhs.longitude_of_AN << std::endl
	   << "   Mean anomaly: " << rhs.mean_anomaly << std::endl
       << "   Circumference: " << rhs.circumference;

    return os;
}

// keplerian orbit elements to cartesian location
template <class TReal>
CartesianLocation<TReal> KOE2Cl(KeplerianOrbitElements<TReal> koe)
{
	// convert to 2D cartesian system:
	TReal x3D = koe.radius * math::calc_cos(koe.mean_anomaly);
	TReal y3D = koe.radius * math::calc_sin(koe.mean_anomaly);

	// rotate by inclination:
	TReal z3D = math::calc_sin(koe.inclination) * y3D;
	y3D = math::calc_cos(koe.inclination) * y3D;

	// rotate by longitude of ascending node:
	TReal xTemp = x3D;
	x3D = math::calc_cos(koe.longitude_of_AN) * xTemp - math::calc_sin(koe.longitude_of_AN) * y3D;
	y3D = math::calc_sin(koe.longitude_of_AN) * xTemp + math::calc_cos(koe.longitude_of_AN) * y3D;

	return CartesianLocation<TReal>(x3D, y3D, z3D);
}

#endif /* ORBIT_HPP */
