#ifndef NODE_HPP
#define NODE_HPP

#include <memory>
#include <iostream>
#include <string>

#include <Vectors.hpp>
#include <Orbit.hpp>
#include <Orbit_Data.hpp>
#include <Math.hpp>

template <class TReal>
class Node
{
public:
	Node(KeplerianOrbitElements<TReal> koe, unsigned int ID=0, Category::NodeType nt=Category::NodeType::Guest)
	: orbit(koe)
	{
		updatePosition();
		this->radianPerSecond	= radPerSecond();
		setSatType();
		setNodetype(nt);
		this->ID = ID;
	}

	// return the circumference in meters
	TReal circumference()
	{
		return this->orbit.circumference;
	}

	// return the velocity in meters/second
	TReal velocity()
	{
		return Physics::velocity(this->orbit.radius);
	}

	// return the period in seconds
	TReal period()
	{
		return this->circumference() / this->velocity();
	}

	void move(TReal seconds)
	{
		orbit.mean_anomaly += radianPerSecond * seconds;
		orbit.mean_anomaly = fmod(orbit.mean_anomaly, constants::two_pi<TReal>());

		updatePosition();
	}

	KeplerianOrbitElements<TReal> const getOrbit()
	{
		return this->orbit;
	}

	SphericalLocation<TReal> const getSL()
	{
		return this->sl3DPosition;
	}

	CartesianLocation<TReal> const getCL()
	{
		return this->cl3DPosition;
	}

	std::string const getSatType_s()
	{
		return this->sat_type_s;
	}

	std::string const getNodeType_s()
	{
		return this->node_type_s;
	}

	Category::SatType getSatType_e()
	{
		return this->sat_type_e;
	}

	Category::NodeType getNodeType_e()
	{
		return this->node_type_e;
	}

	unsigned int getID()
	{
		return this->ID;
	}

	void setOrbit(TReal r, TReal i, TReal l, TReal m)
	{
		this->orbit.set(r, i, l, m);
	}

	void print()
	{
		std::cout << this->orbit  		<< std::endl;
		std::cout << "sl:             " << this->sl3DPosition << std::endl;
		std::cout << "cl:             " << this->cl3DPosition << std::endl;
		std::cout << "velocity:       " << this->velocity() << std::endl;
		std::cout << "period:         " << this->period() << std::endl;
		std::cout << "rad per second: "	<< this->radianPerSecond << std::endl;
	}

	void debug()
	{
		std::cout << "(" << sat_type_s << " " << this->ID << ", " << rad2Deg(this->sl3DPosition.Equat.y) << ", " << rad2Deg(this->sl3DPosition.Equat.z) << ")\n";
	}

private:
	TReal radPerSecond()
	{
		return constants::two_pi<TReal>() / period();
	}

	void setSatType()
	{
		if(orbit.radius > Satellites::MEO::radius) 		this->sat_type_e = Category::SatType::GEO;
		else if(orbit.radius > Satellites::LEO::alt_max + Earth::radius)	this->sat_type_e = Category::SatType::MEO;
		else if(orbit.radius > Earth::radius)			this->sat_type_e = Category::SatType::LEO;
		else this->sat_type_e = Category::SatType::GS;

		this->sat_type_s = Category::ST2string(this->sat_type_e);
	}

	void setNodetype(Category::NodeType nt)
	{
		this->node_type_e = nt;
		this->node_type_s = Category::NT2string(this->node_type_e);
	}

	void updatePosition()
	{
		cl3DPosition = KOE2Cl(orbit);
		sl3DPosition = cartesian2Spherical(cl3DPosition.XYZ);
	}


	KeplerianOrbitElements<TReal> 	orbit;
	SphericalLocation<TReal>	  	sl3DPosition;
	CartesianLocation<TReal>	  	cl3DPosition;

	TReal 							radianPerSecond;

	// Types and ID
	Category::SatType				sat_type_e;
	std::string						sat_type_s;

	Category::NodeType				node_type_e;
	std::string						node_type_s;

	unsigned int 					ID;
};

#endif /* NODE_HPP */
