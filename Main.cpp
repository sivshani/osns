#include <cmath>
#include <cctype>
#include <iostream>
#include <cstring>
#include <stdexcept>
#include <string>
#include <algorithm>

#include <Math_Test.hpp>
#include <Orbit_Test.hpp>
#include <Node_Test.hpp>
#include <Graph.hpp>

// for testing purposes
#include "Node.hpp"

void usage()
{
	std::cout << "Optical Satellite Network Simulator\nUsage: OSNS [--test test mode | --simul] for running the simulation" << std::endl;
}

int main(int argc, char** argv)
{
	if(argc == 1 || argc > 2)
	{
		usage();
	}
	else
	{
		std::string arg(argv[1]);
		// to lower
		std::transform(arg.begin(), arg.end(), arg.begin(), [](unsigned char c){ return std::tolower(c); });

		std::string testStr("--test");
		std::string smlStr("--simul");

		// Test mode
		if(arg == testStr)
		{
			std::cout << "Test Mode" << std::endl;

			unsigned int test = 1;
			math::test_math(test);
			test_orbit(test);
            test_node(test);
        }
		// Command line mode
		else if(arg == smlStr)
		{
			std::cout << "Simulation is running...\nThis might take a while.\nResults will be written to \"out_xxx_xxx.txt\" file." << std::endl;

			const int num = 5;
			int chance[num] = {0, 100, 1000, 5000, 10000};
			for(int i = 0; i < num; i++)
			{
				try
				{
					Graph grph;
					grph.run(chance[i]);
				}
				catch (std::exception& e)
				{
					std::cout << "\nEXCEPTION: " << e.what() << std::endl;
				}
			}
		}
		else
		{
			usage();
		}
	}
}
