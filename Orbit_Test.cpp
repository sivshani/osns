#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <limits>
#include <vector>

#include "boost/multiprecision/mpfr.hpp"
//#include "boost/multiprecision/float128.hpp"

#include <Orbit_Data.hpp>
#include <Orbit_Test.hpp>
#include <Orbit.hpp>
#include <Math.hpp>
#include <Test.hpp>

using namespace boost::math;
using namespace boost::multiprecision;

void test_location(unsigned int& test)
{
	std::cout << "\nTest " << __func__ << ":" << std::endl;

	std::vector<bool> results;

	std::vector<Sl_MPFR_f_1000> argumentsMPFR;
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_15_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_30_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_45_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_60_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_75_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_90_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_105_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_120_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_135_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_150_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_180_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_195_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_210_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_225_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_240_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_255_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_270_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_285_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_300_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_315_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_330_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_345_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_360_M, 0.0));

	Cl_MPFR_f_1000 cl1;
	Sl_MPFR_f_1000 sl1;
	Cl_MPFR_f_1000 cl2;
	Sl_MPFR_f_1000 sl2;

	for(auto &e : argumentsMPFR)
	{
		cl1 = spherical2Cartesian(e.Equat);
		sl1 = cartesian2Spherical(cl1.XYZ);
		cl2 = spherical2Cartesian(sl1.Equat);
		sl2 = cartesian2Spherical(cl2.XYZ);

		std::cout << "test " << test <<": (MPFR)" << std::endl;
		std::cout << "sl1=" << sl1 << "\nsl2=" << sl2 << "\ncl1=" << cl1.XYZ << "\ncl2=" << cl2.XYZ << std::endl;

		if(sl1.Equat == sl2.Equat) midResults(test, true, results);
		else midResults(test, false, results);

		test++;
	}
	finalResults(results);
	results.clear();
	argumentsMPFR.clear();

	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_15_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_30_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_45_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_60_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_75_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_90_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_105_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_120_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_135_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_150_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_165_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_180_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_195_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_210_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_225_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_240_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_255_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_270_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_285_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_300_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_315_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_330_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_345_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, -RAD_360_M, 0.0));

	for(auto &e : argumentsMPFR)
	{
		cl1 = spherical2Cartesian(e.Equat);
		sl1 = cartesian2Spherical(cl1.XYZ);
		cl2 = spherical2Cartesian(sl1.Equat);
		sl2 = cartesian2Spherical(cl2.XYZ);

		std::cout << "test " << test <<": (MPFR)" << std::endl;
		std::cout << "sl1=" << sl1 << "\nsl2=" << sl2 << "\ncl1=" << cl1.XYZ << "\ncl2=" << cl2.XYZ << std::endl;

		if(sl1.Equat == sl2.Equat) midResults(test, true, results);
		else midResults(test, false, results);

		test++;
	}
	finalResults(results);
	results.clear();
	argumentsMPFR.clear();

	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_15_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_30_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_45_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_60_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_75_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_90_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_105_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_120_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_135_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_150_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_165_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_180_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_195_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_210_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_225_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_240_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_255_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_270_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_285_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_300_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_315_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_330_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_345_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_360_M));

	for(auto &e : argumentsMPFR)
	{
		cl1 = spherical2Cartesian(e.Equat);
		sl1 = cartesian2Spherical(cl1.XYZ);
		cl2 = spherical2Cartesian(sl1.Equat);
		sl2 = cartesian2Spherical(cl2.XYZ);

		std::cout << "test " << test <<": (MPFR)" << std::endl;
		std::cout << "sl1=" << sl1 << "\nsl2=" << sl2 << "\ncl1=" << cl1.XYZ << "\ncl2=" << cl2.XYZ << std::endl;

		if(sl1.Equat == sl2.Equat) midResults(test, true, results);
		else midResults(test, false, results);

		test++;
	}
	finalResults(results);
	results.clear();
	argumentsMPFR.clear();

	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_15_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_30_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_45_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_60_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_75_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, RAD_90_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, -RAD_15_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, -RAD_30_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, -RAD_45_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, -RAD_60_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, -RAD_75_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, 0.0, -RAD_90_M));

	for(auto &e : argumentsMPFR)
	{
		cl1 = spherical2Cartesian(e.Equat);
		sl1 = cartesian2Spherical(cl1.XYZ);
		cl2 = spherical2Cartesian(sl1.Equat);
		sl2 = cartesian2Spherical(cl2.XYZ);

		std::cout << "test " << test <<": (MPFR)" << std::endl;
		std::cout << "sl1=" << sl1 << "\nsl2=" << sl2 << "\ncl1=" << cl1.XYZ << "\ncl2=" << cl2.XYZ << std::endl;

		if(sl1.Equat == sl2.Equat) midResults(test, true, results);
		else midResults(test, false, results);

		test++;
	}
	finalResults(results);
	results.clear();
	argumentsMPFR.clear();

	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, 0.0));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, RAD_15_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, RAD_30_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, RAD_45_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, RAD_60_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, RAD_75_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, RAD_90_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, -RAD_15_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, -RAD_30_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, -RAD_45_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, -RAD_60_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, -RAD_75_M));
	argumentsMPFR.push_back(Sl_MPFR_f_1000 (1.0, RAD_165_M, -RAD_90_M));

	for(auto &e : argumentsMPFR)
	{
		cl1 = spherical2Cartesian(e.Equat);
		sl1 = cartesian2Spherical(cl1.XYZ);
		cl2 = spherical2Cartesian(sl1.Equat);
		sl2 = cartesian2Spherical(cl2.XYZ);

		std::cout << "test " << test <<": (MPFR)" << std::endl;
		std::cout << "sl1=" << sl1 << "\nsl2=" << sl2 << "\ncl1=" << cl1.XYZ << "\ncl2=" << cl2.XYZ << std::endl;

		if(sl1.Equat == sl2.Equat) midResults(test, true, results);
		else midResults(test, false, results);

		test++;
	}
	finalResults(results);
	results.clear();
	argumentsMPFR.clear();
}

void test_keplerianOrbitElements(unsigned int& test)
{
	std::cout << "\nTest " << __func__ << ":" << std::endl;

	std::vector<bool> results;

	Sl_MPFR_f_1000 pos_s(mpfr_float_1000(1.0), mpfr_float_1000(0.0), mpfr_float_1000(0.0));
	Cl_MPFR_f_1000 pos_c;
	KOE_MPFR_f_1000 koe(mpfr_float_1000(1.0), mpfr_float_1000(0.0), mpfr_float_1000(0.0), mpfr_float_1000(0.0));
	Sl_MPFR_f_1000 koe2sl;
	Cl_MPFR_f_1000 koe2cl;
	for(int i = 0; i < 24; i++)
	{
		pos_c = spherical2Cartesian(pos_s.Equat);
		koe2cl = KOE2Cl(koe).XYZ;
		koe2sl = cartesian2Spherical(koe2cl.XYZ);

		std::cout << "test " << test <<": (MPFR)" << std::endl;
		std::cout << "sl1=" << pos_s << "\nsl2=" << koe2sl << "\ncl1=" << pos_c << "\ncl2=" << koe2cl << "\nkoe=" << koe <<  std::endl;

		if(pos_s == koe2sl) midResults(test, true, results);
		else midResults(test, false, results);

		pos_s.Equat.y += RAD_15_M;
		koe.mean_anomaly += RAD_15_M;
		test++;
	}
	finalResults(results);
	results.clear();

	pos_s.set(mpfr_float_1000(1.0), mpfr_float_1000(0.0), mpfr_float_1000(0.0));
	koe.set (mpfr_float_1000(1.0), mpfr_float_1000(RAD_90_M), mpfr_float_1000(0.0), mpfr_float_1000(0.0));
	for(int i = 0; i < 24; i++)
	{
		pos_c = spherical2Cartesian(pos_s.Equat);
		koe2cl = KOE2Cl(koe).XYZ;
		koe2sl = cartesian2Spherical(koe2cl.XYZ);

		std::cout << "test " << test <<": (MPFR)" << std::endl;
		std::cout << "sl1=" << pos_s << "\nsl2=" << koe2sl << "\ncl1=" << pos_c << "\ncl2=" << koe2cl << "\nkoe=" << koe <<  std::endl;

		if(pos_c.XYZ == koe2cl.XYZ) midResults(test, true, results);
		else midResults(test, false, results);

		pos_s.Equat.z += RAD_15_M;
		koe.mean_anomaly += RAD_15_M;
		test++;
	}
	finalResults(results);
}

void test_orbit(unsigned int& test)
{
	std::streamsize ss = std::cout.precision(); // store precision
	std::cout.precision(50); // set precision

	test_location(test);
	test_keplerianOrbitElements(test);

	std::cout.precision(ss); // restore precision

}
