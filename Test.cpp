#include <iostream>
#include <iomanip>
#include <cstring>
#include <limits>
#include <vector>
#include <cmath>

void midResults(unsigned int test_number, bool status, std::vector<bool>& results)
{
	std::string p = "\033[1;32mpassed\033[0m";
	std::string f = "\033[1;31mfailed\033[0m";
	std::string s = status ? p : f;

	std::cout << "   test " << test_number << ": " << s << std::endl << "------------------------------" << std::endl;

	results.push_back(status);
}

void finalResults(const std::vector<bool>& results)
{
	std::cout << "Results: " << std::endl;

	unsigned int test = 1;
	std::string status;
	std::string p = "\033[1;32mpassed\033[0m";
	std::string f = "\033[1;31mfailed\033[0m";
	for(auto && item : results)
	{
		status = item ? p : f;

		std::cout << "   Test " << test << ": " << status << std::endl;

		test++;
	}
}
