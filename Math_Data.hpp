#ifndef MATH_DATA_HPP
#define MATH_DATA_HPP

#include <cmath>

#include "boost/multiprecision/mpfr.hpp"

using namespace boost::math;
using namespace boost::multiprecision;


	const double PI_D         = constants::pi<double>();
	const double RAD_360_D    = constants::two_pi<double>();
	const double RAD_240_D    = constants::four_thirds_pi<double>();
	const double RAD_180_D    = PI_D;
	const double RAD_135_D    = constants::three_quarters_pi<double>();
	const double RAD_120_D    = constants::two_thirds_pi<double>();
	const double RAD_90_D     = constants::half_pi<double>();
	const double RAD_60_D     = constants::third_pi<double>();
	const double RAD_45_D     = RAD_135_D - RAD_90_D;
	const double RAD_30_D     = constants::sixth_pi<double>();
	const double RAD_15_D     = RAD_60_D - RAD_45_D;
	const double RAD_75_D     = RAD_60_D + RAD_15_D;
	const double RAD_105_D    = RAD_90_D + RAD_15_D;
	const double RAD_150_D    = RAD_90_D + RAD_60_D;
	const double RAD_165_D    = RAD_120_D + RAD_45_D;
	const double RAD_195_D    = RAD_180_D + RAD_15_D;
	const double RAD_210_D    = RAD_180_D + RAD_30_D;
	const double RAD_225_D    = RAD_180_D + RAD_45_D;
	const double RAD_255_D    = RAD_240_D + RAD_15_D;
	const double RAD_270_D    = RAD_180_D + RAD_90_D;
	const double RAD_285_D    = RAD_240_D + RAD_45_D;
	const double RAD_300_D    = RAD_180_D + RAD_120_D;
	const double RAD_315_D    = RAD_180_D + RAD_135_D;
	const double RAD_330_D    = RAD_240_D + RAD_90_D;
	const double RAD_345_D    = RAD_210_D + RAD_135_D;

	const mpfr_float_1000 PI_M         = constants::pi<mpfr_float_1000>();
	const mpfr_float_1000 RAD_360_M    = constants::two_pi<mpfr_float_1000>();
	const mpfr_float_1000 RAD_240_M    = constants::four_thirds_pi<mpfr_float_1000>();
	const mpfr_float_1000 RAD_180_M    = PI_M;
	const mpfr_float_1000 RAD_135_M    = constants::three_quarters_pi<mpfr_float_1000>();
	const mpfr_float_1000 RAD_120_M    = constants::two_thirds_pi<mpfr_float_1000>();
	const mpfr_float_1000 RAD_90_M     = constants::half_pi<mpfr_float_1000>();
	const mpfr_float_1000 RAD_60_M     = constants::third_pi<mpfr_float_1000>();
	const mpfr_float_1000 RAD_45_M     = RAD_135_M - RAD_90_M;
	const mpfr_float_1000 RAD_30_M     = constants::sixth_pi<mpfr_float_1000>();
	const mpfr_float_1000 RAD_15_M     = RAD_60_M - RAD_45_M;
	const mpfr_float_1000 RAD_75_M     = RAD_60_M + RAD_15_M;
	const mpfr_float_1000 RAD_105_M    = RAD_90_M + RAD_15_M;
	const mpfr_float_1000 RAD_150_M    = RAD_90_M + RAD_60_M;
	const mpfr_float_1000 RAD_165_M    = RAD_120_M + RAD_45_M;
	const mpfr_float_1000 RAD_195_M    = RAD_180_M + RAD_15_M;
	const mpfr_float_1000 RAD_210_M    = RAD_180_M + RAD_30_M;
	const mpfr_float_1000 RAD_225_M    = RAD_180_M + RAD_45_M;
	const mpfr_float_1000 RAD_255_M    = RAD_240_M + RAD_15_M;
	const mpfr_float_1000 RAD_270_M    = RAD_180_M + RAD_90_M;
	const mpfr_float_1000 RAD_285_M    = RAD_240_M + RAD_45_M;
	const mpfr_float_1000 RAD_300_M    = RAD_180_M + RAD_120_M;
	const mpfr_float_1000 RAD_315_M    = RAD_180_M + RAD_135_M;
	const mpfr_float_1000 RAD_330_M    = RAD_240_M + RAD_90_M;
	const mpfr_float_1000 RAD_345_M    = RAD_210_M + RAD_135_M;

	const double SIN_15_D    = (sqrt(6.0) - sqrt(2.0)) / 4.0;
	const double SIN_30_D    = 0.5;
	const double SIN_45_D    = sqrt(2.0) / 2.0;
	const double SIN_60_D    = sqrt(3.0) / 2.0;
	const double SIN_75_D    = (sqrt(6.0) + sqrt(2.0)) / 4.0;
	const double SIN_90_D    = 1.0;
	const double SIN_105_D   = (sqrt(6.0) + sqrt(2.0)) / 4.0;
	const double SIN_120_D   = sqrt(3.0) / 2.0;
	const double SIN_135_D   = sqrt(2.0) / 2.0;
	const double SIN_150_D   = 0.5;
	const double SIN_165_D   = (sqrt(6.0) - sqrt(2.0)) / 4.0;
	const double SIN_180_D   = 0.0;
	const double SIN_195_D   = -(sqrt(6.0) - sqrt(2.0)) / 4.0;
	const double SIN_210_D   = -0.5;
	const double SIN_225_D   = -sqrt(2.0) / 2.0;
	const double SIN_240_D   = -sqrt(3.0) / 2.0;
	const double SIN_255_D   = -(sqrt(6.0) + sqrt(2.0)) / 4.0;
	const double SIN_270_D   = -1.0;
	const double SIN_285_D   = -(sqrt(6.0) + sqrt(2.0)) / 4.0;
	const double SIN_300_D   = -sqrt(3.0) / 2.0;
	const double SIN_315_D   = -sqrt(2.0) / 2;
	const double SIN_330_D   = -0.5;
	const double SIN_345_D   = -(sqrt(6.0) - sqrt(2.0)) / 4.0;
	const double SIN_360_D   = 0.0;

	const mpfr_float_1000 SIN_15_M    = mpfr_float_1000((sqrt(6.0) - sqrt(2.0)) / 4.0);
	const mpfr_float_1000 SIN_30_M    = mpfr_float_1000(0.5);
	const mpfr_float_1000 SIN_45_M    = mpfr_float_1000(sqrt(2.0) / 2.0);
	const mpfr_float_1000 SIN_60_M    = mpfr_float_1000(sqrt(3.0) / 2.0);
	const mpfr_float_1000 SIN_75_M    = mpfr_float_1000((sqrt(6.0) + sqrt(2.0)) / 4.0);
	const mpfr_float_1000 SIN_90_M    = mpfr_float_1000(1.0);
	const mpfr_float_1000 SIN_105_M   = mpfr_float_1000((sqrt(6.0) + sqrt(2.0)) / 4.0);
	const mpfr_float_1000 SIN_120_M   = mpfr_float_1000(sqrt(3.0) / 2.0);
	const mpfr_float_1000 SIN_135_M   = mpfr_float_1000(sqrt(2.0) / 2.0);
	const mpfr_float_1000 SIN_150_M   = mpfr_float_1000(0.5);
	const mpfr_float_1000 SIN_165_M   = mpfr_float_1000((sqrt(6.0) - sqrt(2.0)) / 4.0);
	const mpfr_float_1000 SIN_180_M   = mpfr_float_1000(0.0);
	const mpfr_float_1000 SIN_195_M   = mpfr_float_1000(-(sqrt(6.0) - sqrt(2.0)) / 4.0);
	const mpfr_float_1000 SIN_210_M   = mpfr_float_1000(-0.5);
	const mpfr_float_1000 SIN_225_M   = mpfr_float_1000(-sqrt(2.0) / 2.0);
	const mpfr_float_1000 SIN_240_M   = mpfr_float_1000(-sqrt(3.0) / 2.0);
	const mpfr_float_1000 SIN_255_M   = mpfr_float_1000(-(sqrt(6.0) + sqrt(2.0)) / 4.0);
	const mpfr_float_1000 SIN_270_M   = mpfr_float_1000(-1.0);
	const mpfr_float_1000 SIN_285_M   = mpfr_float_1000(-(sqrt(6.0) + sqrt(2.0)) / 4.0);
	const mpfr_float_1000 SIN_300_M   = mpfr_float_1000(-sqrt(3.0) / 2.0);
	const mpfr_float_1000 SIN_315_M   = mpfr_float_1000(-sqrt(2.0) / 2);
	const mpfr_float_1000 SIN_330_M   = mpfr_float_1000(-0.5);
	const mpfr_float_1000 SIN_345_M   = mpfr_float_1000(-(sqrt(6.0) - sqrt(2.0)) / 4.0);
	const mpfr_float_1000 SIN_360_M   = mpfr_float_1000(0.0);

	const double COS_15_D    = (sqrt(6.0) + sqrt(2.0)) / 4.0;
	const double COS_30_D    = sqrt(3.0) / 2.0;
	const double COS_45_D    = sqrt(2.0) / 2.0;
	const double COS_60_D    = 0.5;
	const double COS_75_D    = (sqrt(6.0) - sqrt(2.0)) / 4.0;
	const double COS_90_D    = 0.0;
	const double COS_105_D   = -(sqrt(6.0) - sqrt(2.0)) / 4.0;
	const double COS_120_D   = -0.5;
	const double COS_135_D   = -sqrt(2.0) / 2.0;
	const double COS_150_D   = -sqrt(3.0) / 2.0;
	const double COS_165_D   = -(sqrt(6.0) + sqrt(2.0)) / 4.0;
	const double COS_180_D   = -1.0;
	const double COS_195_D   = -(sqrt(6.0) + sqrt(2.0)) / 4.0;
	const double COS_210_D   = -sqrt(3.0) / 2.0;
	const double COS_225_D   = -sqrt(2.0) / 2.0;
	const double COS_240_D   = -0.5;
	const double COS_255_D   = -(sqrt(6.0) - sqrt(2.0)) / 4.0;
	const double COS_270_D   = 0.0;
	const double COS_285_D   = (sqrt(6.0) - sqrt(2.0)) / 4.0;
	const double COS_300_D   = 0.5;
	const double COS_315_D   = sqrt(2.0) / 2.0;
	const double COS_330_D   = sqrt(3.0) / 2.0;
	const double COS_345_D   = (sqrt(6.0) + sqrt(2.0)) / 4.0;
	const double COS_360_D   = 1.0;

	const mpfr_float_1000 COS_15_M    = mpfr_float_1000((sqrt(6.0) + sqrt(2.0)) / 4.0);
	const mpfr_float_1000 COS_30_M    = mpfr_float_1000(sqrt(3.0) / 2.0);
	const mpfr_float_1000 COS_45_M    = mpfr_float_1000(sqrt(2.0) / 2.0);
	const mpfr_float_1000 COS_60_M    = mpfr_float_1000(0.5);
	const mpfr_float_1000 COS_75_M    = mpfr_float_1000((sqrt(6.0) - sqrt(2.0)) / 4.0);
	const mpfr_float_1000 COS_90_M    = mpfr_float_1000(0.0);
	const mpfr_float_1000 COS_105_M   = mpfr_float_1000(-(sqrt(6.0) - sqrt(2.0)) / 4.0);
	const mpfr_float_1000 COS_120_M   = mpfr_float_1000(-0.5);
	const mpfr_float_1000 COS_135_M   = mpfr_float_1000(-sqrt(2.0) / 2.0);
	const mpfr_float_1000 COS_150_M   = mpfr_float_1000(-sqrt(3.0) / 2.0);
	const mpfr_float_1000 COS_165_M   = mpfr_float_1000(-(sqrt(6.0) + sqrt(2.0)) / 4.0);
	const mpfr_float_1000 COS_180_M   = mpfr_float_1000(-1.0);
	const mpfr_float_1000 COS_195_M   = mpfr_float_1000(-(sqrt(6.0) + sqrt(2.0)) / 4.0);
	const mpfr_float_1000 COS_210_M   = mpfr_float_1000(-sqrt(3.0) / 2.0);
	const mpfr_float_1000 COS_225_M   = mpfr_float_1000(-sqrt(2.0) / 2.0);
	const mpfr_float_1000 COS_240_M   = mpfr_float_1000(-0.5);
	const mpfr_float_1000 COS_255_M   = mpfr_float_1000(-(sqrt(6.0) - sqrt(2.0)) / 4.0);
	const mpfr_float_1000 COS_270_M   = mpfr_float_1000(0.0);
	const mpfr_float_1000 COS_285_M   = mpfr_float_1000((sqrt(6.0) - sqrt(2.0)) / 4.0);
	const mpfr_float_1000 COS_300_M   = mpfr_float_1000(0.5);
	const mpfr_float_1000 COS_315_M   = mpfr_float_1000(sqrt(2.0) / 2.0);
	const mpfr_float_1000 COS_330_M   = mpfr_float_1000(sqrt(3.0) / 2.0);
	const mpfr_float_1000 COS_345_M   = mpfr_float_1000((sqrt(6.0) + sqrt(2.0)) / 4.0);
	const mpfr_float_1000 COS_360_M   = mpfr_float_1000(1.0);


#endif /* MATH_DATA_HPP */
