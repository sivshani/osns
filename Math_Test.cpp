#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <limits>
#include <vector>

#include "boost/multiprecision/mpfr.hpp"

#include <Math_Test.hpp>
#include <Math.hpp>
#include <Test.hpp>

using namespace boost::math;
using namespace boost::multiprecision;

namespace math
{
	void test_rad2Deg(unsigned int& test)
	{
		std::cout << "\nTest " << __func__ << ":" << std::endl;

		std::vector<bool> results;

		// double arguments ***(argument, expected result)***
		std::vector<std::pair<double, double>> argumentsDouble;

		// test double, mod2PI=true
		argumentsDouble.push_back(std::make_pair(double(0.0), double(0.0)));

		argumentsDouble.push_back(std::make_pair(RAD_15_D, double(15.0)));
		argumentsDouble.push_back(std::make_pair(RAD_30_D, double(30.0)));
		argumentsDouble.push_back(std::make_pair(RAD_45_D, double(45.0)));
		argumentsDouble.push_back(std::make_pair(RAD_60_D, double(60.0)));
		argumentsDouble.push_back(std::make_pair(RAD_75_D, double(75.0)));
		argumentsDouble.push_back(std::make_pair(RAD_90_D, double(90.0)));
		argumentsDouble.push_back(std::make_pair(RAD_105_D, double(105.0)));
		argumentsDouble.push_back(std::make_pair(RAD_120_D, double(120.0)));
		argumentsDouble.push_back(std::make_pair(RAD_135_D, double(135.0)));
		argumentsDouble.push_back(std::make_pair(RAD_150_D, double(150.0)));
		argumentsDouble.push_back(std::make_pair(RAD_165_D, double(165.0)));
		argumentsDouble.push_back(std::make_pair(RAD_180_D, double(180.0)));
		argumentsDouble.push_back(std::make_pair(RAD_195_D, double(195.0)));
		argumentsDouble.push_back(std::make_pair(RAD_210_D, double(210.0)));
		argumentsDouble.push_back(std::make_pair(RAD_225_D, double(225.0)));
		argumentsDouble.push_back(std::make_pair(RAD_240_D, double(240.0)));
		argumentsDouble.push_back(std::make_pair(RAD_255_D, double(255.0)));
		argumentsDouble.push_back(std::make_pair(RAD_270_D, double(270.0)));
		argumentsDouble.push_back(std::make_pair(RAD_285_D, double(285.0)));
		argumentsDouble.push_back(std::make_pair(RAD_300_D, double(300.0)));
		argumentsDouble.push_back(std::make_pair(RAD_315_D, double(315.0)));
		argumentsDouble.push_back(std::make_pair(RAD_330_D, double(330.0)));
		argumentsDouble.push_back(std::make_pair(RAD_345_D, double(345.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D, double(0.0)));

		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_15_D, double(15.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_30_D, double(30.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_45_D, double(45.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_60_D, double(60.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_75_D, double(75.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_90_D, double(90.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_105_D, double(105.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_120_D, double(120.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_135_D, double(135.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_150_D, double(150.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_165_D, double(165.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_180_D, double(180.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_195_D, double(195.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_210_D, double(210.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_225_D, double(225.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_240_D, double(240.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_255_D, double(255.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_270_D, double(270.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_285_D, double(285.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_300_D, double(300.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_315_D, double(315.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_330_D, double(330.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_345_D, double(345.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_360_D, double(0.0)));

		argumentsDouble.push_back(std::make_pair(double(0.00000000000001), double(5.7295779513082321e-13)));

		argumentsDouble.push_back(std::make_pair(double(-0.0), double(-0.0)));

		argumentsDouble.push_back(std::make_pair(-RAD_15_D, double(-15.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_30_D, double(-30.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_45_D, double(-45.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_60_D, double(-60.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_75_D, double(-75.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_90_D, double(-90.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_105_D, double(-105.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_120_D, double(-120.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_135_D, double(-135.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_150_D, double(-150.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_165_D, double(-165.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_180_D, double(-180.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_195_D, double(-195.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_210_D, double(-210.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_225_D, double(-225.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_240_D, double(-240.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_255_D, double(-255.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_270_D, double(-270.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_285_D, double(-285.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_300_D, double(-300.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_315_D, double(-315.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_330_D, double(-330.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_345_D, double(-345.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D, double(-0.0)));

		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_15_D, double(-15.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_30_D, double(-30.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_45_D, double(-45.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_60_D, double(-60.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_75_D, double(-75.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_90_D, double(-90.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_105_D, double(-105.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_120_D, double(-120.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_135_D, double(-135.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_150_D, double(-150.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_165_D, double(-165.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_180_D, double(-180.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_195_D, double(-195.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_210_D, double(-210.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_225_D, double(-225.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_240_D, double(-240.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_255_D, double(-255.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_270_D, double(-270.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_285_D, double(-285.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_300_D, double(-300.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_315_D, double(-315.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_330_D, double(-330.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_345_D, double(-345.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_360_D, double(-0.0)));

		argumentsDouble.push_back(std::make_pair(double(-0.00000000000001), double(-5.7295779513082321e-13)));

		for(auto &pair : argumentsDouble)
		{
			auto result = rad2Deg(pair.first);

			std::cout << "	test " << test <<": (Double, mod2PI=true)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     " expect deg=" << pair.second << std::endl <<
						 " result deg=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);

			test++;
		}
		argumentsDouble.clear();

		// test double, mod2PI=false
		argumentsDouble.push_back(std::make_pair(double(0.0), double(0.0)));

		argumentsDouble.push_back(std::make_pair(RAD_15_D, double(15.0)));
		argumentsDouble.push_back(std::make_pair(RAD_30_D, double(30.0)));
		argumentsDouble.push_back(std::make_pair(RAD_45_D, double(45.0)));
		argumentsDouble.push_back(std::make_pair(RAD_60_D, double(60.0)));
		argumentsDouble.push_back(std::make_pair(RAD_75_D, double(75.0)));
		argumentsDouble.push_back(std::make_pair(RAD_90_D, double(90.0)));
		argumentsDouble.push_back(std::make_pair(RAD_105_D, double(105.0)));
		argumentsDouble.push_back(std::make_pair(RAD_120_D, double(120.0)));
		argumentsDouble.push_back(std::make_pair(RAD_135_D, double(135.0)));
		argumentsDouble.push_back(std::make_pair(RAD_150_D, double(150.0)));
		argumentsDouble.push_back(std::make_pair(RAD_165_D, double(165.0)));
		argumentsDouble.push_back(std::make_pair(RAD_180_D, double(180.0)));
		argumentsDouble.push_back(std::make_pair(RAD_195_D, double(195.0)));
		argumentsDouble.push_back(std::make_pair(RAD_210_D, double(210.0)));
		argumentsDouble.push_back(std::make_pair(RAD_225_D, double(225.0)));
		argumentsDouble.push_back(std::make_pair(RAD_240_D, double(240.0)));
		argumentsDouble.push_back(std::make_pair(RAD_255_D, double(255.0)));
		argumentsDouble.push_back(std::make_pair(RAD_270_D, double(270.0)));
		argumentsDouble.push_back(std::make_pair(RAD_285_D, double(285.0)));
		argumentsDouble.push_back(std::make_pair(RAD_300_D, double(300.0)));
		argumentsDouble.push_back(std::make_pair(RAD_315_D, double(315.0)));
		argumentsDouble.push_back(std::make_pair(RAD_330_D, double(330.0)));
		argumentsDouble.push_back(std::make_pair(RAD_345_D, double(345.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D, double(360.0)));

		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_15_D, double(360.0 + 15.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_30_D, double(360.0 + 30.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_45_D, double(360.0 + 45.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_60_D, double(360.0 + 60.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_75_D, double(360.0 + 75.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_90_D, double(360.0 + 90.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_105_D, double(360.0 + 105.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_120_D, double(360.0 + 120.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_135_D, double(360.0 + 135.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_150_D, double(360.0 + 150.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_165_D, double(360.0 + 165.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_180_D, double(360.0 + 180.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_195_D, double(360.0 + 195.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_210_D, double(360.0 + 210.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_225_D, double(360.0 + 225.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_240_D, double(360.0 + 240.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_255_D, double(360.0 + 255.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_270_D, double(360.0 + 270.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_285_D, double(360.0 + 285.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_300_D, double(360.0 + 300.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_315_D, double(360.0 + 315.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_330_D, double(360.0 + 330.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_345_D, double(360.0 + 345.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_360_D, double(360.0 + 360.0)));

		argumentsDouble.push_back(std::make_pair(double(-0.0), double(-0.0)));

		argumentsDouble.push_back(std::make_pair(-RAD_15_D, double(-15.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_30_D, double(-30.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_45_D, double(-45.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_60_D, double(-60.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_75_D, double(-75.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_90_D, double(-90.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_105_D, double(-105.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_120_D, double(-120.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_135_D, double(-135.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_150_D, double(-150.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_165_D, double(-165.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_180_D, double(-180.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_195_D, double(-195.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_210_D, double(-210.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_225_D, double(-225.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_240_D, double(-240.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_255_D, double(-255.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_270_D, double(-270.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_285_D, double(-285.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_300_D, double(-300.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_315_D, double(-315.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_330_D, double(-330.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_345_D, double(-345.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D, double(-360.0)));

		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_15_D, double(-360.0 - 15.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_30_D, double(-360.0 - 30.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_45_D, double(-360.0 - 45.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_60_D, double(-360.0 - 60.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_75_D, double(-360.0 - 75.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_90_D, double(-360.0 - 90.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_105_D, double(-360.0 - 105.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_120_D, double(-360.0 - 120.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_135_D, double(-360.0 - 135.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_150_D, double(-360.0 - 150.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_165_D, double(-360.0 - 165.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_180_D, double(-360.0 - 180.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_195_D, double(-360.0 - 195.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_210_D, double(-360.0 - 210.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_225_D, double(-360.0 - 225.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_240_D, double(-360.0 - 240.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_255_D, double(-360.0 - 255.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_270_D, double(-360.0 - 270.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_285_D, double(-360.0 - 285.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_300_D, double(-360.0 - 300.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_315_D, double(-360.0 - 315.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_330_D, double(-360.0 - 330.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_345_D, double(-360.0 - 345.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_360_D, double(-360.0 - 360.0)));

		for(auto &pair : argumentsDouble)
		{
			auto result = rad2Deg(pair.first, false);

			std::cout << "	test " << test <<": (Double, mod2PI=false)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     " expect deg=" << pair.second << std::endl <<
						 " result deg=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);

			test++;
		}
		argumentsDouble.clear();

		// MPFR1000 arguments ***(argument, expected result)***
		std::vector<std::pair<mpfr_float_1000, mpfr_float_1000>> argumentsMPFR1000;

		// test MPFR1000, mod2PI=true
		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(0.0), mpfr_float_1000(0.0)));

		argumentsMPFR1000.push_back(std::make_pair(RAD_15_M, mpfr_float_1000(15.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_30_M, mpfr_float_1000(30.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_45_M, mpfr_float_1000(45.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_60_M, mpfr_float_1000(60.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_75_M, mpfr_float_1000(75.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_90_M, mpfr_float_1000(90.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_105_M, mpfr_float_1000(105.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_120_M, mpfr_float_1000(120.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_135_M, mpfr_float_1000(135.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_150_M, mpfr_float_1000(150.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_165_M, mpfr_float_1000(165.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_180_M, mpfr_float_1000(180.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_195_M, mpfr_float_1000(195.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_210_M, mpfr_float_1000(210.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_225_M, mpfr_float_1000(225.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_240_M, mpfr_float_1000(240.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_255_M, mpfr_float_1000(255.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_270_M, mpfr_float_1000(270.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_285_M, mpfr_float_1000(285.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_300_M, mpfr_float_1000(300.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_315_M, mpfr_float_1000(315.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_330_M, mpfr_float_1000(330.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_345_M, mpfr_float_1000(345.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M, mpfr_float_1000(0.0)));

		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_15_M, mpfr_float_1000(15.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_30_M, mpfr_float_1000(30.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_45_M, mpfr_float_1000(45.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_60_M, mpfr_float_1000(60.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_75_M, mpfr_float_1000(75.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_90_M, mpfr_float_1000(90.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_105_M, mpfr_float_1000(105.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_120_M, mpfr_float_1000(120.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_135_M, mpfr_float_1000(135.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_150_M, mpfr_float_1000(150.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_165_M, mpfr_float_1000(165.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_180_M, mpfr_float_1000(180.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_195_M, mpfr_float_1000(195.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_210_M, mpfr_float_1000(210.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_225_M, mpfr_float_1000(225.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_240_M, mpfr_float_1000(240.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_255_M, mpfr_float_1000(255.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_270_M, mpfr_float_1000(270.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_285_M, mpfr_float_1000(285.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_300_M, mpfr_float_1000(300.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_315_M, mpfr_float_1000(315.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_330_M, mpfr_float_1000(330.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_345_M, mpfr_float_1000(345.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_360_M, mpfr_float_1000(0.0)));

		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(0.00000000000001), mpfr_float_1000(5.7295779513082321e-13)));

		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(-0.0), mpfr_float_1000(-0.0)));

		argumentsMPFR1000.push_back(std::make_pair(-RAD_15_M, mpfr_float_1000(-15.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_30_M, mpfr_float_1000(-30.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_45_M, mpfr_float_1000(-45.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_60_M, mpfr_float_1000(-60.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_75_M, mpfr_float_1000(-75.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_90_M, mpfr_float_1000(-90.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_105_M, mpfr_float_1000(-105.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_120_M, mpfr_float_1000(-120.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_135_M, mpfr_float_1000(-135.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_150_M, mpfr_float_1000(-150.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_165_M, mpfr_float_1000(-165.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_180_M, mpfr_float_1000(-180.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_195_M, mpfr_float_1000(-195.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_210_M, mpfr_float_1000(-210.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_225_M, mpfr_float_1000(-225.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_240_M, mpfr_float_1000(-240.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_255_M, mpfr_float_1000(-255.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_270_M, mpfr_float_1000(-270.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_285_M, mpfr_float_1000(-285.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_300_M, mpfr_float_1000(-300.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_315_M, mpfr_float_1000(-315.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_330_M, mpfr_float_1000(-330.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_345_M, mpfr_float_1000(-345.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M, mpfr_float_1000(-0.0)));

		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_15_M, mpfr_float_1000(-15.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_30_M, mpfr_float_1000(-30.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_45_M, mpfr_float_1000(-45.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_60_M, mpfr_float_1000(-60.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_75_M, mpfr_float_1000(-75.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_90_M, mpfr_float_1000(-90.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_105_M, mpfr_float_1000(-105.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_120_M, mpfr_float_1000(-120.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_135_M, mpfr_float_1000(-135.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_150_M, mpfr_float_1000(-150.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_165_M, mpfr_float_1000(-165.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_180_M, mpfr_float_1000(-180.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_195_M, mpfr_float_1000(-195.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_210_M, mpfr_float_1000(-210.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_225_M, mpfr_float_1000(-225.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_240_M, mpfr_float_1000(-240.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_255_M, mpfr_float_1000(-255.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_270_M, mpfr_float_1000(-270.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_285_M, mpfr_float_1000(-285.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_300_M, mpfr_float_1000(-300.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_315_M, mpfr_float_1000(-315.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_330_M, mpfr_float_1000(-330.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_345_M, mpfr_float_1000(-345.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_360_M, mpfr_float_1000(-0.0)));

		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(-0.00000000000001), mpfr_float_1000(-5.7295779513082321e-13)));

		for(auto &pair : argumentsMPFR1000)
		{
			auto result = rad2Deg(pair.first);

			std::cout << "	test " << test <<": (MPFR1000, mod2PI=true)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     " expect deg=" << pair.second << std::endl <<
						 " result deg=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);

			test++;
		}
		argumentsMPFR1000.clear();

		// test MPFR1000, mod2PI=false
		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(0.0), mpfr_float_1000(0.0)));

		argumentsMPFR1000.push_back(std::make_pair(RAD_15_M, mpfr_float_1000(15.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_30_M, mpfr_float_1000(30.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_45_M, mpfr_float_1000(45.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_60_M, mpfr_float_1000(60.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_75_M, mpfr_float_1000(75.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_90_M, mpfr_float_1000(90.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_105_M, mpfr_float_1000(105.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_120_M, mpfr_float_1000(120.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_135_M, mpfr_float_1000(135.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_150_M, mpfr_float_1000(150.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_165_M, mpfr_float_1000(165.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_180_M, mpfr_float_1000(180.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_195_M, mpfr_float_1000(195.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_210_M, mpfr_float_1000(210.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_225_M, mpfr_float_1000(225.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_240_M, mpfr_float_1000(240.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_255_M, mpfr_float_1000(255.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_270_M, mpfr_float_1000(270.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_285_M, mpfr_float_1000(285.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_300_M, mpfr_float_1000(300.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_315_M, mpfr_float_1000(315.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_330_M, mpfr_float_1000(330.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_345_M, mpfr_float_1000(345.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M, mpfr_float_1000(360.0)));

		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_15_M, mpfr_float_1000(360.0 + 15.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_30_M, mpfr_float_1000(360.0 + 30.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_45_M, mpfr_float_1000(360.0 + 45.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_60_M, mpfr_float_1000(360.0 + 60.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_75_M, mpfr_float_1000(360.0 + 75.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_90_M, mpfr_float_1000(360.0 + 90.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_105_M, mpfr_float_1000(360.0 + 105.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_120_M, mpfr_float_1000(360.0 + 120.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_135_M, mpfr_float_1000(360.0 + 135.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_150_M, mpfr_float_1000(360.0 + 150.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_165_M, mpfr_float_1000(360.0 + 165.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_180_M, mpfr_float_1000(360.0 + 180.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_195_M, mpfr_float_1000(360.0 + 195.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_210_M, mpfr_float_1000(360.0 + 210.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_225_M, mpfr_float_1000(360.0 + 225.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_240_M, mpfr_float_1000(360.0 + 240.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_255_M, mpfr_float_1000(360.0 + 255.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_270_M, mpfr_float_1000(360.0 + 270.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_285_M, mpfr_float_1000(360.0 + 285.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_300_M, mpfr_float_1000(360.0 + 300.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_315_M, mpfr_float_1000(360.0 + 315.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_330_M, mpfr_float_1000(360.0 + 330.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_345_M, mpfr_float_1000(360.0 + 345.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_360_M, mpfr_float_1000(360.0 + 360.0)));

		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(-0.0), mpfr_float_1000(-0.0)));

		argumentsMPFR1000.push_back(std::make_pair(-RAD_15_M, mpfr_float_1000(-15.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_30_M, mpfr_float_1000(-30.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_45_M, mpfr_float_1000(-45.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_60_M, mpfr_float_1000(-60.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_75_M, mpfr_float_1000(-75.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_90_M, mpfr_float_1000(-90.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_105_M, mpfr_float_1000(-105.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_120_M, mpfr_float_1000(-120.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_135_M, mpfr_float_1000(-135.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_150_M, mpfr_float_1000(-150.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_165_M, mpfr_float_1000(-165.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_180_M, mpfr_float_1000(-180.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_195_M, mpfr_float_1000(-195.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_210_M, mpfr_float_1000(-210.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_225_M, mpfr_float_1000(-225.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_240_M, mpfr_float_1000(-240.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_255_M, mpfr_float_1000(-255.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_270_M, mpfr_float_1000(-270.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_285_M, mpfr_float_1000(-285.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_300_M, mpfr_float_1000(-300.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_315_M, mpfr_float_1000(-315.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_330_M, mpfr_float_1000(-330.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_345_M, mpfr_float_1000(-345.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M, mpfr_float_1000(-360.0)));

		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_15_M, mpfr_float_1000(-360.0 - 15.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_30_M, mpfr_float_1000(-360.0 - 30.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_45_M, mpfr_float_1000(-360.0 - 45.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_60_M, mpfr_float_1000(-360.0 - 60.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_75_M, mpfr_float_1000(-360.0 - 75.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_90_M, mpfr_float_1000(-360.0 - 90.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_105_M, mpfr_float_1000(-360.0 - 105.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_120_M, mpfr_float_1000(-360.0 - 120.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_135_M, mpfr_float_1000(-360.0 - 135.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_150_M, mpfr_float_1000(-360.0 - 150.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_165_M, mpfr_float_1000(-360.0 - 165.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_180_M, mpfr_float_1000(-360.0 - 180.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_195_M, mpfr_float_1000(-360.0 - 195.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_210_M, mpfr_float_1000(-360.0 - 210.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_225_M, mpfr_float_1000(-360.0 - 225.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_240_M, mpfr_float_1000(-360.0 - 240.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_255_M, mpfr_float_1000(-360.0 - 255.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_270_M, mpfr_float_1000(-360.0 - 270.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_285_M, mpfr_float_1000(-360.0 - 285.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_300_M, mpfr_float_1000(-360.0 - 300.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_315_M, mpfr_float_1000(-360.0 - 315.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_330_M, mpfr_float_1000(-360.0 - 330.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_345_M, mpfr_float_1000(-360.0 - 345.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_360_M, mpfr_float_1000(-360.0 - 360.0)));

		for(auto &pair : argumentsMPFR1000)
		{
			auto result = rad2Deg(pair.first, false);

			std::cout << "	test " << test <<": (MPFR1000, mod2PI=false)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     " expect deg=" << pair.second << std::endl <<
						 " result deg=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);

			test++;
		}
		argumentsMPFR1000.clear();


		finalResults(results);
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------------------

	void test_deg2Rad(unsigned int& test)
	{
		std::cout << "\nTest " << __func__ << ":" << std::endl;

		std::vector<bool> results;

		// double arguments ***(expected result, argument)***
		std::vector<std::pair<double, double>> argumentsDouble;

		// test double, mod2PI=true
		argumentsDouble.push_back(std::make_pair(double(0.0), double(0.0)));

		argumentsDouble.push_back(std::make_pair(RAD_15_D, double(15.0)));
		argumentsDouble.push_back(std::make_pair(RAD_30_D, double(30.0)));
		argumentsDouble.push_back(std::make_pair(RAD_45_D, double(45.0)));
		argumentsDouble.push_back(std::make_pair(RAD_60_D, double(60.0)));
		argumentsDouble.push_back(std::make_pair(RAD_75_D, double(75.0)));
		argumentsDouble.push_back(std::make_pair(RAD_90_D, double(90.0)));
		argumentsDouble.push_back(std::make_pair(RAD_105_D, double(105.0)));
		argumentsDouble.push_back(std::make_pair(RAD_120_D, double(120.0)));
		argumentsDouble.push_back(std::make_pair(RAD_135_D, double(135.0)));
		argumentsDouble.push_back(std::make_pair(RAD_150_D, double(150.0)));
		argumentsDouble.push_back(std::make_pair(RAD_165_D, double(165.0)));
		argumentsDouble.push_back(std::make_pair(RAD_180_D, double(180.0)));
		argumentsDouble.push_back(std::make_pair(RAD_195_D, double(195.0)));
		argumentsDouble.push_back(std::make_pair(RAD_210_D, double(210.0)));
		argumentsDouble.push_back(std::make_pair(RAD_225_D, double(225.0)));
		argumentsDouble.push_back(std::make_pair(RAD_240_D, double(240.0)));
		argumentsDouble.push_back(std::make_pair(RAD_255_D, double(255.0)));
		argumentsDouble.push_back(std::make_pair(RAD_270_D, double(270.0)));
		argumentsDouble.push_back(std::make_pair(RAD_285_D, double(285.0)));
		argumentsDouble.push_back(std::make_pair(RAD_300_D, double(300.0)));
		argumentsDouble.push_back(std::make_pair(RAD_315_D, double(315.0)));
		argumentsDouble.push_back(std::make_pair(RAD_330_D, double(330.0)));
		argumentsDouble.push_back(std::make_pair(RAD_345_D, double(345.0)));
		argumentsDouble.push_back(std::make_pair(0.0, double(360.0)));

		argumentsDouble.push_back(std::make_pair(RAD_15_D, double(360.0 + 15.0)));
		argumentsDouble.push_back(std::make_pair(RAD_30_D, double(360.0 + 30.0)));
		argumentsDouble.push_back(std::make_pair(RAD_45_D, double(360.0 + 45.0)));
		argumentsDouble.push_back(std::make_pair(RAD_60_D, double(360.0 + 60.0)));
		argumentsDouble.push_back(std::make_pair(RAD_75_D, double(360.0 + 75.0)));
		argumentsDouble.push_back(std::make_pair(RAD_90_D, double(360.0 + 90.0)));
		argumentsDouble.push_back(std::make_pair(RAD_105_D, double(360.0 + 105.0)));
		argumentsDouble.push_back(std::make_pair(RAD_120_D, double(360.0 + 120.0)));
		argumentsDouble.push_back(std::make_pair(RAD_135_D, double(360.0 + 135.0)));
		argumentsDouble.push_back(std::make_pair(RAD_150_D, double(360.0 + 150.0)));
		argumentsDouble.push_back(std::make_pair(RAD_165_D, double(360.0 + 165.0)));
		argumentsDouble.push_back(std::make_pair(RAD_180_D, double(360.0 + 180.0)));
		argumentsDouble.push_back(std::make_pair(RAD_195_D, double(360.0 + 195.0)));
		argumentsDouble.push_back(std::make_pair(RAD_210_D, double(360.0 + 210.0)));
		argumentsDouble.push_back(std::make_pair(RAD_225_D, double(360.0 + 225.0)));
		argumentsDouble.push_back(std::make_pair(RAD_240_D, double(360.0 + 240.0)));
		argumentsDouble.push_back(std::make_pair(RAD_255_D, double(360.0 + 255.0)));
		argumentsDouble.push_back(std::make_pair(RAD_270_D, double(360.0 + 270.0)));
		argumentsDouble.push_back(std::make_pair(RAD_285_D, double(360.0 + 285.0)));
		argumentsDouble.push_back(std::make_pair(RAD_300_D, double(360.0 + 300.0)));
		argumentsDouble.push_back(std::make_pair(RAD_315_D, double(360.0 + 315.0)));
		argumentsDouble.push_back(std::make_pair(RAD_330_D, double(360.0 + 330.0)));
		argumentsDouble.push_back(std::make_pair(RAD_345_D, double(360.0 + 345.0)));
		argumentsDouble.push_back(std::make_pair(0.0, double(360.0 + 360.0)));

		argumentsDouble.push_back(std::make_pair(double(0.00000000000001), double(5.7295779513082321e-13)));

		argumentsDouble.push_back(std::make_pair(double(-0.0), double(-0.0)));

		argumentsDouble.push_back(std::make_pair(-RAD_15_D, double(-15.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_30_D, double(-30.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_45_D, double(-45.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_60_D, double(-60.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_75_D, double(-75.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_90_D, double(-90.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_105_D, double(-105.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_120_D, double(-120.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_135_D, double(-135.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_150_D, double(-150.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_165_D, double(-165.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_180_D, double(-180.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_195_D, double(-195.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_210_D, double(-210.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_225_D, double(-225.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_240_D, double(-240.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_255_D, double(-255.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_270_D, double(-270.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_285_D, double(-285.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_300_D, double(-300.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_315_D, double(-315.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_330_D, double(-330.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_345_D, double(-345.0)));
		argumentsDouble.push_back(std::make_pair(-0.0, double(-0.0)));

		argumentsDouble.push_back(std::make_pair(- RAD_15_D, double(-360.0 -15.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_30_D, double(-360.0 -30.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_45_D, double(-360.0 -45.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_60_D, double(-360.0 -60.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_75_D, double(-360.0 -75.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_90_D, double(-360.0 -90.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_105_D, double(-360.0 -105.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_120_D, double(-360.0 -120.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_135_D, double(-360.0 -135.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_150_D, double(-360.0 -150.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_165_D, double(-360.0 -165.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_180_D, double(-360.0 -180.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_195_D, double(-360.0 -195.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_210_D, double(-360.0 -210.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_225_D, double(-360.0 -225.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_240_D, double(-360.0 -240.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_255_D, double(-360.0 -255.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_270_D, double(-360.0 -270.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_285_D, double(-360.0 -285.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_300_D, double(-360.0 -300.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_315_D, double(-360.0 -315.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_330_D, double(-360.0 -330.0)));
		argumentsDouble.push_back(std::make_pair(- RAD_345_D, double(-360.0 -345.0)));
		argumentsDouble.push_back(std::make_pair(0.0, double(-360.0 -360.0)));

		argumentsDouble.push_back(std::make_pair(double(-0.00000000000001), double(-5.7295779513082321e-13)));

		for(auto &pair : argumentsDouble)
		{
			auto result = deg2Rad(pair.second);

			std::cout << "	test " << test <<": (Double, mod2PI=true)" << std::endl;
			std::cout << "	deg=" << pair.second << std::endl <<
					     " expect rad=" << pair.first << std::endl <<
						 " result deg=" << result << std::endl;

			if(isApproxEqual(result, pair.first)) midResults(test, true, results);
			else midResults(test, false, results);

			test++;
		}
		argumentsDouble.clear();

		// test double, mod2PI=false
		argumentsDouble.push_back(std::make_pair(double(0.0), double(0.0)));

		argumentsDouble.push_back(std::make_pair(RAD_15_D, double(15.0)));
		argumentsDouble.push_back(std::make_pair(RAD_30_D, double(30.0)));
		argumentsDouble.push_back(std::make_pair(RAD_45_D, double(45.0)));
		argumentsDouble.push_back(std::make_pair(RAD_60_D, double(60.0)));
		argumentsDouble.push_back(std::make_pair(RAD_75_D, double(75.0)));
		argumentsDouble.push_back(std::make_pair(RAD_90_D, double(90.0)));
		argumentsDouble.push_back(std::make_pair(RAD_105_D, double(105.0)));
		argumentsDouble.push_back(std::make_pair(RAD_120_D, double(120.0)));
		argumentsDouble.push_back(std::make_pair(RAD_135_D, double(135.0)));
		argumentsDouble.push_back(std::make_pair(RAD_150_D, double(150.0)));
		argumentsDouble.push_back(std::make_pair(RAD_165_D, double(165.0)));
		argumentsDouble.push_back(std::make_pair(RAD_180_D, double(180.0)));
		argumentsDouble.push_back(std::make_pair(RAD_195_D, double(195.0)));
		argumentsDouble.push_back(std::make_pair(RAD_210_D, double(210.0)));
		argumentsDouble.push_back(std::make_pair(RAD_225_D, double(225.0)));
		argumentsDouble.push_back(std::make_pair(RAD_240_D, double(240.0)));
		argumentsDouble.push_back(std::make_pair(RAD_255_D, double(255.0)));
		argumentsDouble.push_back(std::make_pair(RAD_270_D, double(270.0)));
		argumentsDouble.push_back(std::make_pair(RAD_285_D, double(285.0)));
		argumentsDouble.push_back(std::make_pair(RAD_300_D, double(300.0)));
		argumentsDouble.push_back(std::make_pair(RAD_315_D, double(315.0)));
		argumentsDouble.push_back(std::make_pair(RAD_330_D, double(330.0)));
		argumentsDouble.push_back(std::make_pair(RAD_345_D, double(345.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D, double(360.0)));

		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_15_D, double(360.0 + 15.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_30_D, double(360.0 + 30.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_45_D, double(360.0 + 45.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_60_D, double(360.0 + 60.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_75_D, double(360.0 + 75.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_90_D, double(360.0 + 90.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_105_D, double(360.0 + 105.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_120_D, double(360.0 + 120.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_135_D, double(360.0 + 135.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_150_D, double(360.0 + 150.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_165_D, double(360.0 + 165.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_180_D, double(360.0 + 180.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_195_D, double(360.0 + 195.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_210_D, double(360.0 + 210.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_225_D, double(360.0 + 225.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_240_D, double(360.0 + 240.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_255_D, double(360.0 + 255.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_270_D, double(360.0 + 270.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_285_D, double(360.0 + 285.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_300_D, double(360.0 + 300.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_315_D, double(360.0 + 315.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_330_D, double(360.0 + 330.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_345_D, double(360.0 + 345.0)));
		argumentsDouble.push_back(std::make_pair(RAD_360_D + RAD_360_D, double(360.0 + 360.0)));

		argumentsDouble.push_back(std::make_pair(double(-0.0), double(-0.0)));

		argumentsDouble.push_back(std::make_pair(-RAD_15_D, double(-15.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_30_D, double(-30.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_45_D, double(-45.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_60_D, double(-60.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_75_D, double(-75.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_90_D, double(-90.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_105_D, double(-105.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_120_D, double(-120.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_135_D, double(-135.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_150_D, double(-150.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_165_D, double(-165.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_180_D, double(-180.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_195_D, double(-195.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_210_D, double(-210.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_225_D, double(-225.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_240_D, double(-240.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_255_D, double(-255.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_270_D, double(-270.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_285_D, double(-285.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_300_D, double(-300.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_315_D, double(-315.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_330_D, double(-330.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_345_D, double(-345.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D, double(-360.0)));

		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_15_D, double(-360.0 - 15.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_30_D, double(-360.0 - 30.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_45_D, double(-360.0 - 45.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_60_D, double(-360.0 - 60.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_75_D, double(-360.0 - 75.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_90_D, double(-360.0 - 90.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_105_D, double(-360.0 - 105.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_120_D, double(-360.0 - 120.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_135_D, double(-360.0 - 135.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_150_D, double(-360.0 - 150.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_165_D, double(-360.0 - 165.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_180_D, double(-360.0 - 180.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_195_D, double(-360.0 - 195.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_210_D, double(-360.0 - 210.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_225_D, double(-360.0 - 225.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_240_D, double(-360.0 - 240.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_255_D, double(-360.0 - 255.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_270_D, double(-360.0 - 270.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_285_D, double(-360.0 - 285.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_300_D, double(-360.0 - 300.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_315_D, double(-360.0 - 315.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_330_D, double(-360.0 - 330.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_345_D, double(-360.0 - 345.0)));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D - RAD_360_D, double(-360.0 - 360.0)));

		for(auto &pair : argumentsDouble)
		{
			auto result = deg2Rad(pair.second, false);

			std::cout << "	test " << test <<": (Double, mod2PI=false)" << std::endl;
			std::cout << "	deg=" << pair.second << std::endl <<
					     " expect rad=" << pair.first << std::endl <<
						 " result deg=" << result << std::endl;

			if(isApproxEqual(result, pair.first)) midResults(test, true, results);
			else midResults(test, false, results);

			test++;
		}
		argumentsDouble.clear();

		// MPFR1000 arguments ***(expected result, argument)***
		std::vector<std::pair<mpfr_float_1000, mpfr_float_1000>> argumentsMPFR1000;

		// test MPFR1000, mod2PI=true
		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(0.0), mpfr_float_1000(0.0)));

		argumentsMPFR1000.push_back(std::make_pair(RAD_15_M, mpfr_float_1000(15.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_30_M, mpfr_float_1000(30.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_45_M, mpfr_float_1000(45.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_60_M, mpfr_float_1000(60.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_75_M, mpfr_float_1000(75.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_90_M, mpfr_float_1000(90.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_105_M, mpfr_float_1000(105.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_120_M, mpfr_float_1000(120.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_135_M, mpfr_float_1000(135.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_150_M, mpfr_float_1000(150.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_165_M, mpfr_float_1000(165.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_180_M, mpfr_float_1000(180.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_195_M, mpfr_float_1000(195.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_210_M, mpfr_float_1000(210.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_225_M, mpfr_float_1000(225.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_240_M, mpfr_float_1000(240.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_255_M, mpfr_float_1000(255.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_270_M, mpfr_float_1000(270.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_285_M, mpfr_float_1000(285.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_300_M, mpfr_float_1000(300.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_315_M, mpfr_float_1000(315.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_330_M, mpfr_float_1000(330.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_345_M, mpfr_float_1000(345.0)));
		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(0.0), mpfr_float_1000(360.0)));

		argumentsMPFR1000.push_back(std::make_pair(RAD_15_M, mpfr_float_1000(360.0 + 15.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_30_M, mpfr_float_1000(360.0 + 30.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_45_M, mpfr_float_1000(360.0 + 45.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_60_M, mpfr_float_1000(360.0 + 60.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_75_M, mpfr_float_1000(360.0 + 75.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_90_M, mpfr_float_1000(360.0 + 90.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_105_M, mpfr_float_1000(360.0 + 105.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_120_M, mpfr_float_1000(360.0 + 120.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_135_M, mpfr_float_1000(360.0 + 135.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_150_M, mpfr_float_1000(360.0 + 150.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_165_M, mpfr_float_1000(360.0 + 165.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_180_M, mpfr_float_1000(360.0 + 180.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_195_M, mpfr_float_1000(360.0 + 195.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_210_M, mpfr_float_1000(360.0 + 210.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_225_M, mpfr_float_1000(360.0 + 225.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_240_M, mpfr_float_1000(360.0 + 240.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_255_M, mpfr_float_1000(360.0 + 255.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_270_M, mpfr_float_1000(360.0 + 270.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_285_M, mpfr_float_1000(360.0 + 285.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_300_M, mpfr_float_1000(360.0 + 300.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_315_M, mpfr_float_1000(360.0 + 315.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_330_M, mpfr_float_1000(360.0 + 330.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_345_M, mpfr_float_1000(360.0 + 345.0)));
		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(0.0), mpfr_float_1000(360.0 + 360.0)));

		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(0.00000000000001), mpfr_float_1000(5.7295779513082321e-13)));

		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(-0.0), mpfr_float_1000(-0.0)));

		argumentsMPFR1000.push_back(std::make_pair(-RAD_15_M, mpfr_float_1000(-15.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_30_M, mpfr_float_1000(-30.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_45_M, mpfr_float_1000(-45.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_60_M, mpfr_float_1000(-60.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_75_M, mpfr_float_1000(-75.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_90_M, mpfr_float_1000(-90.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_105_M, mpfr_float_1000(-105.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_120_M, mpfr_float_1000(-120.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_135_M, mpfr_float_1000(-135.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_150_M, mpfr_float_1000(-150.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_165_M, mpfr_float_1000(-165.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_180_M, mpfr_float_1000(-180.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_195_M, mpfr_float_1000(-195.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_210_M, mpfr_float_1000(-210.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_225_M, mpfr_float_1000(-225.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_240_M, mpfr_float_1000(-240.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_255_M, mpfr_float_1000(-255.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_270_M, mpfr_float_1000(-270.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_285_M, mpfr_float_1000(-285.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_300_M, mpfr_float_1000(-300.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_315_M, mpfr_float_1000(-315.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_330_M, mpfr_float_1000(-330.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_345_M, mpfr_float_1000(-345.0)));
		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(0.0), mpfr_float_1000(-360.0)));

		argumentsMPFR1000.push_back(std::make_pair(- RAD_15_M, mpfr_float_1000(-360.0 -15.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_30_M, mpfr_float_1000(-360.0 -30.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_45_M, mpfr_float_1000(-360.0 -45.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_60_M, mpfr_float_1000(-360.0 -60.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_75_M, mpfr_float_1000(-360.0 -75.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_90_M, mpfr_float_1000(-360.0 -90.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_105_M, mpfr_float_1000(-360.0 -105.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_120_M, mpfr_float_1000(-360.0 -120.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_135_M, mpfr_float_1000(-360.0 -135.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_150_M, mpfr_float_1000(-360.0 -150.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_165_M, mpfr_float_1000(-360.0 -165.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_180_M, mpfr_float_1000(-360.0 -180.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_195_M, mpfr_float_1000(-360.0 -195.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_210_M, mpfr_float_1000(-360.0 -210.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_225_M, mpfr_float_1000(-360.0 -225.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_240_M, mpfr_float_1000(-360.0 -240.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_255_M, mpfr_float_1000(-360.0 -255.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_270_M, mpfr_float_1000(-360.0 -270.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_285_M, mpfr_float_1000(-360.0 -285.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_300_M, mpfr_float_1000(-360.0 -300.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_315_M, mpfr_float_1000(-360.0 -315.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_330_M, mpfr_float_1000(-360.0 -330.0)));
		argumentsMPFR1000.push_back(std::make_pair(- RAD_345_M, mpfr_float_1000(-360.0 -345.0)));
		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(0.0), mpfr_float_1000(-360.0 -360.0)));

		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(-0.00000000000001), mpfr_float_1000(-5.7295779513082321e-13)));

		for(auto &pair : argumentsMPFR1000)
		{
			auto result = deg2Rad(pair.second);

			std::cout << "	test " << test <<": (MPFR1000, mod2PI=true)" << std::endl;
			std::cout << "	deg=" << pair.second << std::endl <<
					     " expect rad=" << pair.first << std::endl <<
						 " result deg=" << result << std::endl;

			if(isApproxEqual(result, pair.first)) midResults(test, true, results);
			else midResults(test, false, results);

			test++;
		}
		argumentsMPFR1000.clear();

		// test MPFR1000, mod2PI=false
		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(0.0), mpfr_float_1000(0.0)));

		argumentsMPFR1000.push_back(std::make_pair(RAD_15_M, mpfr_float_1000(15.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_30_M, mpfr_float_1000(30.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_45_M, mpfr_float_1000(45.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_60_M, mpfr_float_1000(60.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_75_M, mpfr_float_1000(75.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_90_M, mpfr_float_1000(90.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_105_M, mpfr_float_1000(105.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_120_M, mpfr_float_1000(120.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_135_M, mpfr_float_1000(135.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_150_M, mpfr_float_1000(150.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_165_M, mpfr_float_1000(165.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_180_M, mpfr_float_1000(180.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_195_M, mpfr_float_1000(195.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_210_M, mpfr_float_1000(210.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_225_M, mpfr_float_1000(225.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_240_M, mpfr_float_1000(240.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_255_M, mpfr_float_1000(255.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_270_M, mpfr_float_1000(270.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_285_M, mpfr_float_1000(285.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_300_M, mpfr_float_1000(300.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_315_M, mpfr_float_1000(315.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_330_M, mpfr_float_1000(330.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_345_M, mpfr_float_1000(345.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M, mpfr_float_1000(360.0)));

		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_15_M, mpfr_float_1000(360.0 + 15.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_30_M, mpfr_float_1000(360.0 + 30.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_45_M, mpfr_float_1000(360.0 + 45.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_60_M, mpfr_float_1000(360.0 + 60.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_75_M, mpfr_float_1000(360.0 + 75.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_90_M, mpfr_float_1000(360.0 + 90.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_105_M, mpfr_float_1000(360.0 + 105.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_120_M, mpfr_float_1000(360.0 + 120.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_135_M, mpfr_float_1000(360.0 + 135.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_150_M, mpfr_float_1000(360.0 + 150.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_165_M, mpfr_float_1000(360.0 + 165.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_180_M, mpfr_float_1000(360.0 + 180.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_195_M, mpfr_float_1000(360.0 + 195.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_210_M, mpfr_float_1000(360.0 + 210.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_225_M, mpfr_float_1000(360.0 + 225.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_240_M, mpfr_float_1000(360.0 + 240.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_255_M, mpfr_float_1000(360.0 + 255.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_270_M, mpfr_float_1000(360.0 + 270.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_285_M, mpfr_float_1000(360.0 + 285.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_300_M, mpfr_float_1000(360.0 + 300.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_315_M, mpfr_float_1000(360.0 + 315.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_330_M, mpfr_float_1000(360.0 + 330.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_345_M, mpfr_float_1000(360.0 + 345.0)));
		argumentsMPFR1000.push_back(std::make_pair(RAD_360_M + RAD_360_M, mpfr_float_1000(360.0 + 360.0)));

		argumentsMPFR1000.push_back(std::make_pair(mpfr_float_1000(-0.0), mpfr_float_1000(-0.0)));

		argumentsMPFR1000.push_back(std::make_pair(-RAD_15_M, mpfr_float_1000(-15.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_30_M, mpfr_float_1000(-30.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_45_M, mpfr_float_1000(-45.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_60_M, mpfr_float_1000(-60.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_75_M, mpfr_float_1000(-75.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_90_M, mpfr_float_1000(-90.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_105_M, mpfr_float_1000(-105.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_120_M, mpfr_float_1000(-120.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_135_M, mpfr_float_1000(-135.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_150_M, mpfr_float_1000(-150.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_165_M, mpfr_float_1000(-165.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_180_M, mpfr_float_1000(-180.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_195_M, mpfr_float_1000(-195.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_210_M, mpfr_float_1000(-210.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_225_M, mpfr_float_1000(-225.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_240_M, mpfr_float_1000(-240.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_255_M, mpfr_float_1000(-255.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_270_M, mpfr_float_1000(-270.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_285_M, mpfr_float_1000(-285.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_300_M, mpfr_float_1000(-300.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_315_M, mpfr_float_1000(-315.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_330_M, mpfr_float_1000(-330.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_345_M, mpfr_float_1000(-345.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M, mpfr_float_1000(-360.0)));

		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_15_M, mpfr_float_1000(-360.0 - 15.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_30_M, mpfr_float_1000(-360.0 - 30.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_45_M, mpfr_float_1000(-360.0 - 45.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_60_M, mpfr_float_1000(-360.0 - 60.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_75_M, mpfr_float_1000(-360.0 - 75.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_90_M, mpfr_float_1000(-360.0 - 90.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_105_M, mpfr_float_1000(-360.0 - 105.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_120_M, mpfr_float_1000(-360.0 - 120.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_135_M, mpfr_float_1000(-360.0 - 135.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_150_M, mpfr_float_1000(-360.0 - 150.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_165_M, mpfr_float_1000(-360.0 - 165.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_180_M, mpfr_float_1000(-360.0 - 180.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_195_M, mpfr_float_1000(-360.0 - 195.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_210_M, mpfr_float_1000(-360.0 - 210.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_225_M, mpfr_float_1000(-360.0 - 225.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_240_M, mpfr_float_1000(-360.0 - 240.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_255_M, mpfr_float_1000(-360.0 - 255.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_270_M, mpfr_float_1000(-360.0 - 270.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_285_M, mpfr_float_1000(-360.0 - 285.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_300_M, mpfr_float_1000(-360.0 - 300.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_315_M, mpfr_float_1000(-360.0 - 315.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_330_M, mpfr_float_1000(-360.0 - 330.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_345_M, mpfr_float_1000(-360.0 - 345.0)));
		argumentsMPFR1000.push_back(std::make_pair(-RAD_360_M - RAD_360_M, mpfr_float_1000(-360.0 - 360.0)));

		for(auto &pair : argumentsMPFR1000)
		{
			auto result = deg2Rad(pair.second, false);

			std::cout << "	test " << test <<": (MPFR1000, mod2PI=false)" << std::endl;
			std::cout << "	deg=" << pair.second << std::endl <<
					     " expect rad=" << pair.first << std::endl <<
						 " result deg=" << result << std::endl;

			if(isApproxEqual(result, pair.first)) midResults(test, true, results);
			else midResults(test, false, results);

			test++;
		}
		argumentsMPFR1000.clear();


		finalResults(results);

	}
	//---------------------------------------------------------------------------------------------------------------------------------------------------------

	void test_sin(unsigned int& test)
	{
		std::cout << "\nTest " << __func__ << ":" << std::endl;

		std::vector<bool> results;

		// double arguments ***(expected argument, result)***
		std::vector<std::pair<double, double>> argumentsDouble;

		// test double
		argumentsDouble.push_back(std::make_pair(0.0, 0.0));
		argumentsDouble.push_back(std::make_pair(RAD_15_D, SIN_15_D));
		argumentsDouble.push_back(std::make_pair(RAD_30_D, SIN_30_D));
		argumentsDouble.push_back(std::make_pair(RAD_45_D, SIN_45_D));
		argumentsDouble.push_back(std::make_pair(RAD_60_D, SIN_60_D));
		argumentsDouble.push_back(std::make_pair(RAD_75_D, SIN_75_D));
		argumentsDouble.push_back(std::make_pair(RAD_90_D, SIN_90_D));
		argumentsDouble.push_back(std::make_pair(RAD_105_D, SIN_105_D));
		argumentsDouble.push_back(std::make_pair(RAD_120_D, SIN_120_D));
		argumentsDouble.push_back(std::make_pair(RAD_135_D, SIN_135_D));
		argumentsDouble.push_back(std::make_pair(RAD_150_D, SIN_150_D));
		argumentsDouble.push_back(std::make_pair(RAD_165_D, SIN_165_D));
		argumentsDouble.push_back(std::make_pair(RAD_180_D, SIN_180_D));
		argumentsDouble.push_back(std::make_pair(RAD_195_D, SIN_195_D));
		argumentsDouble.push_back(std::make_pair(RAD_210_D, SIN_210_D));
		argumentsDouble.push_back(std::make_pair(RAD_225_D, SIN_225_D));
		argumentsDouble.push_back(std::make_pair(RAD_240_D, SIN_240_D));
		argumentsDouble.push_back(std::make_pair(RAD_255_D, SIN_255_D));
		argumentsDouble.push_back(std::make_pair(RAD_270_D, SIN_270_D));
		argumentsDouble.push_back(std::make_pair(RAD_285_D, SIN_285_D));
		argumentsDouble.push_back(std::make_pair(RAD_300_D, SIN_300_D));
		argumentsDouble.push_back(std::make_pair(RAD_315_D, SIN_315_D));
		argumentsDouble.push_back(std::make_pair(RAD_330_D, SIN_330_D));
		argumentsDouble.push_back(std::make_pair(RAD_345_D, SIN_345_D));
		argumentsDouble.push_back(std::make_pair(RAD_360_D, SIN_360_D));

		argumentsDouble.push_back(std::make_pair(0.0, 0.0));
		argumentsDouble.push_back(std::make_pair(-RAD_15_D, -SIN_15_D));
		argumentsDouble.push_back(std::make_pair(-RAD_30_D, -SIN_30_D));
		argumentsDouble.push_back(std::make_pair(-RAD_45_D, -SIN_45_D));
		argumentsDouble.push_back(std::make_pair(-RAD_60_D, -SIN_60_D));
		argumentsDouble.push_back(std::make_pair(-RAD_75_D, -SIN_75_D));
		argumentsDouble.push_back(std::make_pair(-RAD_90_D, -SIN_90_D));
		argumentsDouble.push_back(std::make_pair(-RAD_105_D, -SIN_105_D));
		argumentsDouble.push_back(std::make_pair(-RAD_120_D, -SIN_120_D));
		argumentsDouble.push_back(std::make_pair(-RAD_135_D, -SIN_135_D));
		argumentsDouble.push_back(std::make_pair(-RAD_150_D, -SIN_150_D));
		argumentsDouble.push_back(std::make_pair(-RAD_165_D, -SIN_165_D));
		argumentsDouble.push_back(std::make_pair(-RAD_180_D, SIN_180_D));
		argumentsDouble.push_back(std::make_pair(-RAD_195_D, -SIN_195_D));
		argumentsDouble.push_back(std::make_pair(-RAD_210_D, -SIN_210_D));
		argumentsDouble.push_back(std::make_pair(-RAD_225_D, -SIN_225_D));
		argumentsDouble.push_back(std::make_pair(-RAD_240_D, -SIN_240_D));
		argumentsDouble.push_back(std::make_pair(-RAD_255_D, -SIN_255_D));
		argumentsDouble.push_back(std::make_pair(-RAD_270_D, -SIN_270_D));
		argumentsDouble.push_back(std::make_pair(-RAD_285_D, -SIN_285_D));
		argumentsDouble.push_back(std::make_pair(-RAD_300_D, -SIN_300_D));
		argumentsDouble.push_back(std::make_pair(-RAD_315_D, -SIN_315_D));
		argumentsDouble.push_back(std::make_pair(-RAD_330_D, -SIN_330_D));
		argumentsDouble.push_back(std::make_pair(-RAD_345_D, -SIN_345_D));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D, SIN_360_D));


		// MPFR arguments
		std::vector<std::pair<mpfr_float_1000, mpfr_float_1000>> argumentsMPFR;

		// test double
		argumentsMPFR.push_back(std::make_pair(0.0, 0.0));
		argumentsMPFR.push_back(std::make_pair(RAD_15_M, SIN_15_M));
		argumentsMPFR.push_back(std::make_pair(RAD_30_M, SIN_30_M));
		argumentsMPFR.push_back(std::make_pair(RAD_45_M, SIN_45_M));
		argumentsMPFR.push_back(std::make_pair(RAD_60_M, SIN_60_M));
		argumentsMPFR.push_back(std::make_pair(RAD_75_M, SIN_75_M));
		argumentsMPFR.push_back(std::make_pair(RAD_90_M, SIN_90_M));
		argumentsMPFR.push_back(std::make_pair(RAD_105_M, SIN_105_M));
		argumentsMPFR.push_back(std::make_pair(RAD_120_M, SIN_120_M));
		argumentsMPFR.push_back(std::make_pair(RAD_135_M, SIN_135_M));
		argumentsMPFR.push_back(std::make_pair(RAD_150_M, SIN_150_M));
		argumentsMPFR.push_back(std::make_pair(RAD_165_M, SIN_165_M));
		argumentsMPFR.push_back(std::make_pair(RAD_180_M, SIN_180_M));
		argumentsMPFR.push_back(std::make_pair(RAD_195_M, SIN_195_M));
		argumentsMPFR.push_back(std::make_pair(RAD_210_M, SIN_210_M));
		argumentsMPFR.push_back(std::make_pair(RAD_225_M, SIN_225_M));
		argumentsMPFR.push_back(std::make_pair(RAD_240_M, SIN_240_M));
		argumentsMPFR.push_back(std::make_pair(RAD_255_M, SIN_255_M));
		argumentsMPFR.push_back(std::make_pair(RAD_270_M, SIN_270_M));
		argumentsMPFR.push_back(std::make_pair(RAD_285_M, SIN_285_M));
		argumentsMPFR.push_back(std::make_pair(RAD_300_M, SIN_300_M));
		argumentsMPFR.push_back(std::make_pair(RAD_315_M, SIN_315_M));
		argumentsMPFR.push_back(std::make_pair(RAD_330_M, SIN_330_M));
		argumentsMPFR.push_back(std::make_pair(RAD_345_M, SIN_345_M));
		argumentsMPFR.push_back(std::make_pair(RAD_360_M, SIN_360_M));

		argumentsMPFR.push_back(std::make_pair(0.0, 0.0));
		argumentsMPFR.push_back(std::make_pair(-RAD_15_M, -SIN_15_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_30_M, -SIN_30_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_45_M, -SIN_45_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_60_M, -SIN_60_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_75_M, -SIN_75_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_90_M, -SIN_90_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_105_M, -SIN_105_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_120_M, -SIN_120_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_135_M, -SIN_135_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_150_M, -SIN_150_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_165_M, -SIN_165_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_180_M, -SIN_180_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_195_M, -SIN_195_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_210_M, -SIN_210_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_225_M, -SIN_225_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_240_M, -SIN_240_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_255_M, -SIN_255_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_270_M, -SIN_270_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_285_M, -SIN_285_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_300_M, -SIN_300_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_315_M, -SIN_315_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_330_M, -SIN_330_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_345_M, -SIN_345_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_360_M, -SIN_360_M));

		for(auto &pair : argumentsDouble)
		{
			double result = calc_sin(pair.first);

			std::cout << "	test " << test <<": (Double calc)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect sin=" << pair.second << std::endl <<
						 " result sin=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;

			result = calc_sin1(pair.first);

			std::cout << "	test " << test <<": (Double calc1)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect sin=" << pair.second << std::endl <<
						 " result sin=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;

			result = calc_sin2(pair.first);

			std::cout << "	test " << test <<": (Double calc2)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect sin=" << pair.second << std::endl <<
						 " result sin=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;
		}
		finalResults(results);
		results.clear();

		for(auto &pair : argumentsMPFR)
		{
			mpfr_float_1000 result = calc_sin(pair.first);

			std::cout << "	test " << test <<": (MPFR calc)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect sin=" << pair.second << std::endl <<
						 " result sin=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;

			result = calc_sin1(pair.first);

			std::cout << "	test " << test <<": (MPFR calc1)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect sin=" << pair.second << std::endl <<
						 " result sin=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;

			result = calc_sin2(pair.first);

			std::cout << "	test " << test <<": (MPFR calc2)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect sin=" << pair.second << std::endl <<
						 " result sin=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;
		}
		finalResults(results);
		results.clear();
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------------------

	void test_cos(unsigned int& test)
	{
		std::cout << "\nTest " << __func__ << ":" << std::endl;

		std::vector<bool> results;

		// double arguments ***(expected argument, result)***
		std::vector<std::pair<double, double>> argumentsDouble;

		// test double
		argumentsDouble.push_back(std::make_pair(0.0, 1.0));
		argumentsDouble.push_back(std::make_pair(RAD_15_D, COS_15_D));
		argumentsDouble.push_back(std::make_pair(RAD_30_D, COS_30_D));
		argumentsDouble.push_back(std::make_pair(RAD_45_D, COS_45_D));
		argumentsDouble.push_back(std::make_pair(RAD_60_D, COS_60_D));
		argumentsDouble.push_back(std::make_pair(RAD_75_D, COS_75_D));
		argumentsDouble.push_back(std::make_pair(RAD_90_D, COS_90_D));
		argumentsDouble.push_back(std::make_pair(RAD_105_D, COS_105_D));
		argumentsDouble.push_back(std::make_pair(RAD_120_D, COS_120_D));
		argumentsDouble.push_back(std::make_pair(RAD_135_D, COS_135_D));
		argumentsDouble.push_back(std::make_pair(RAD_150_D, COS_150_D));
		argumentsDouble.push_back(std::make_pair(RAD_165_D, COS_165_D));
		argumentsDouble.push_back(std::make_pair(RAD_180_D, COS_180_D));
		argumentsDouble.push_back(std::make_pair(RAD_195_D, COS_195_D));
		argumentsDouble.push_back(std::make_pair(RAD_210_D, COS_210_D));
		argumentsDouble.push_back(std::make_pair(RAD_225_D, COS_225_D));
		argumentsDouble.push_back(std::make_pair(RAD_240_D, COS_240_D));
		argumentsDouble.push_back(std::make_pair(RAD_255_D, COS_255_D));
		argumentsDouble.push_back(std::make_pair(RAD_270_D, COS_270_D));
		argumentsDouble.push_back(std::make_pair(RAD_285_D, COS_285_D));
		argumentsDouble.push_back(std::make_pair(RAD_300_D, COS_300_D));
		argumentsDouble.push_back(std::make_pair(RAD_315_D, COS_315_D));
		argumentsDouble.push_back(std::make_pair(RAD_330_D, COS_330_D));
		argumentsDouble.push_back(std::make_pair(RAD_345_D, COS_345_D));
		argumentsDouble.push_back(std::make_pair(RAD_360_D, COS_360_D));

		argumentsDouble.push_back(std::make_pair(0.0, 1.0));
		argumentsDouble.push_back(std::make_pair(-RAD_15_D, COS_15_D));
		argumentsDouble.push_back(std::make_pair(-RAD_30_D, COS_30_D));
		argumentsDouble.push_back(std::make_pair(-RAD_45_D, COS_45_D));
		argumentsDouble.push_back(std::make_pair(-RAD_60_D, COS_60_D));
		argumentsDouble.push_back(std::make_pair(-RAD_75_D, COS_75_D));
		argumentsDouble.push_back(std::make_pair(-RAD_90_D, COS_90_D));
		argumentsDouble.push_back(std::make_pair(-RAD_105_D, COS_105_D));
		argumentsDouble.push_back(std::make_pair(-RAD_120_D, COS_120_D));
		argumentsDouble.push_back(std::make_pair(-RAD_135_D, COS_135_D));
		argumentsDouble.push_back(std::make_pair(-RAD_150_D, COS_150_D));
		argumentsDouble.push_back(std::make_pair(-RAD_165_D, COS_165_D));
		argumentsDouble.push_back(std::make_pair(-RAD_180_D, COS_180_D));
		argumentsDouble.push_back(std::make_pair(-RAD_195_D, COS_195_D));
		argumentsDouble.push_back(std::make_pair(-RAD_210_D, COS_210_D));
		argumentsDouble.push_back(std::make_pair(-RAD_225_D, COS_225_D));
		argumentsDouble.push_back(std::make_pair(-RAD_240_D, COS_240_D));
		argumentsDouble.push_back(std::make_pair(-RAD_255_D, COS_255_D));
		argumentsDouble.push_back(std::make_pair(-RAD_270_D, COS_270_D));
		argumentsDouble.push_back(std::make_pair(-RAD_285_D, COS_285_D));
		argumentsDouble.push_back(std::make_pair(-RAD_300_D, COS_300_D));
		argumentsDouble.push_back(std::make_pair(-RAD_315_D, COS_315_D));
		argumentsDouble.push_back(std::make_pair(-RAD_330_D, COS_330_D));
		argumentsDouble.push_back(std::make_pair(-RAD_345_D, COS_345_D));
		argumentsDouble.push_back(std::make_pair(-RAD_360_D, COS_360_D));


		// MPFR arguments
		std::vector<std::pair<mpfr_float_1000, mpfr_float_1000>> argumentsMPFR;

		// test double
		argumentsMPFR.push_back(std::make_pair(0.0, 1.0));
		argumentsMPFR.push_back(std::make_pair(RAD_15_M, COS_15_M));
		argumentsMPFR.push_back(std::make_pair(RAD_30_M, COS_30_M));
		argumentsMPFR.push_back(std::make_pair(RAD_45_M, COS_45_M));
		argumentsMPFR.push_back(std::make_pair(RAD_60_M, COS_60_M));
		argumentsMPFR.push_back(std::make_pair(RAD_75_M, COS_75_M));
		argumentsMPFR.push_back(std::make_pair(RAD_90_M, COS_90_M));
		argumentsMPFR.push_back(std::make_pair(RAD_105_M, COS_105_M));
		argumentsMPFR.push_back(std::make_pair(RAD_120_M, COS_120_M));
		argumentsMPFR.push_back(std::make_pair(RAD_135_M, COS_135_M));
		argumentsMPFR.push_back(std::make_pair(RAD_150_M, COS_150_M));
		argumentsMPFR.push_back(std::make_pair(RAD_165_M, COS_165_M));
		argumentsMPFR.push_back(std::make_pair(RAD_180_M, COS_180_M));
		argumentsMPFR.push_back(std::make_pair(RAD_195_M, COS_195_M));
		argumentsMPFR.push_back(std::make_pair(RAD_210_M, COS_210_M));
		argumentsMPFR.push_back(std::make_pair(RAD_225_M, COS_225_M));
		argumentsMPFR.push_back(std::make_pair(RAD_240_M, COS_240_M));
		argumentsMPFR.push_back(std::make_pair(RAD_255_M, COS_255_M));
		argumentsMPFR.push_back(std::make_pair(RAD_270_M, COS_270_M));
		argumentsMPFR.push_back(std::make_pair(RAD_285_M, COS_285_M));
		argumentsMPFR.push_back(std::make_pair(RAD_300_M, COS_300_M));
		argumentsMPFR.push_back(std::make_pair(RAD_315_M, COS_315_M));
		argumentsMPFR.push_back(std::make_pair(RAD_330_M, COS_330_M));
		argumentsMPFR.push_back(std::make_pair(RAD_345_M, COS_345_M));
		argumentsMPFR.push_back(std::make_pair(RAD_360_M, COS_360_M));

		argumentsMPFR.push_back(std::make_pair(0.0, 1.0));
		argumentsMPFR.push_back(std::make_pair(-RAD_15_M, COS_15_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_30_M, COS_30_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_45_M, COS_45_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_60_M, COS_60_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_75_M, COS_75_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_90_M, COS_90_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_105_M, COS_105_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_120_M, COS_120_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_135_M, COS_135_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_150_M, COS_150_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_165_M, COS_165_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_180_M, COS_180_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_195_M, COS_195_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_210_M, COS_210_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_225_M, COS_225_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_240_M, COS_240_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_255_M, COS_255_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_270_M, COS_270_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_285_M, COS_285_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_300_M, COS_300_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_315_M, COS_315_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_330_M, COS_330_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_345_M, COS_345_M));
		argumentsMPFR.push_back(std::make_pair(-RAD_360_M, COS_360_M));

		for(auto &pair : argumentsDouble)
		{
			double result = calc_cos(pair.first);

			std::cout << "	test " << test <<": (Double calc)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect cos=" << pair.second << std::endl <<
						 " result cos=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;

			result = calc_cos1(pair.first);

			std::cout << "	test " << test <<": (Double calc1)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect cos=" << pair.second << std::endl <<
						 " result cos=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;

			result = calc_cos2(pair.first);

			std::cout << "	test " << test <<": (Double calc2)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect cos=" << pair.second << std::endl <<
						 " result cos=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;
		}
		finalResults(results);
		results.clear();

		for(auto &pair : argumentsMPFR)
		{
			mpfr_float_1000 result = calc_cos(pair.first);

			std::cout << "	test " << test <<": (MPFR calc)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect cos=" << pair.second << std::endl <<
						 " result cos=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;

			result = calc_cos1(pair.first);

			std::cout << "	test " << test <<": (MPFR calc1)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect cos=" << pair.second << std::endl <<
						 " result cos=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;

			result = calc_cos2(pair.first);

			std::cout << "	test " << test <<": (MPFR calc2)" << std::endl;
			std::cout << "	rad=" << pair.first << std::endl <<
					     "	deg=" << rad2Deg(pair.first) << std::endl <<
						 " expect cos=" << pair.second << std::endl <<
						 " result cos=" << result << std::endl;

			if(isApproxEqual(result, pair.second)) midResults(test, true, results);
			else midResults(test, false, results);
			test++;
		}
		finalResults(results);
		results.clear();
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------------------

	void test_math(unsigned int& test)
	{
		std::streamsize ss = std::cout.precision(); // store precision
		std::cout.precision(50); // set precision

		test_rad2Deg(test);
		test_deg2Rad(test);
		test_sin(test);
		test_cos(test);

		std::cout.precision(ss); // restore precision
	}
}
