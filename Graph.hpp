#ifndef GRAPH_HPP
#define GRPAH_HPP

#include <vector>
#include <set>
#include <memory>
#include <algorithm>
#include<iterator>
#include <fstream>
#include <ctime>

#include "boost/multiprecision/mpfr.hpp"
#include "Constellation.hpp"

using namespace boost::math;
using namespace boost::multiprecision;

//#define ENABLE_DEBUG_H // comment out to disable debugging


class Data
{
public:
	mpfr_float_1000 OCTsetUpTime = mpfr_float_1000(15.0); // seconds
	mpfr_float_1000 OnSatSetUpTime = mpfr_float_1000(1.0); // seconds

	mpfr_float_1000 allTimeInt; // seconds
	mpfr_float_1000 allUpTime; // seconds
	mpfr_float_1000 breaks;
	mpfr_float_1000 maxPropTime = mpfr_float_1000(0.0);
	std::vector<mpfr_float_1000> propTime;

	// interval times : max path
	std::vector<std::pair<mpfr_float_1000, mpfr_float_1000>> intTimes;
	// for setup times 15
	std::vector<mpfr_float_1000> upTimes;
	std::vector<int> disconnected;

	mpfr_float_1000 maxUpTime = mpfr_float_1000(0.0);
	mpfr_float_1000 minUpTime = mpfr_float_1000(0.0);
	mpfr_float_1000 avUpTime = mpfr_float_1000(0.0);

	void calc()
	{
#ifdef ENABLE_DEBUG_H
std::cerr << "DEBUG " << __func__ << ":" << std::endl;
#endif

		if(0 == breaks)
		{
			allUpTime = maxUpTime = minUpTime = avUpTime = allTimeInt;
		}

		for(auto &e : intTimes)
		{
			mpfr_float_1000 propTime = mpfr_float_1000(e.second / Physics::speedOfLight);
			this->propTime.push_back(propTime);

			if(propTime > maxPropTime) maxPropTime = propTime;

			mpfr_float_1000 t = e.first - OCTsetUpTime - propTime - OnSatSetUpTime;
			if (t < 0) t = 0;

			upTimes.push_back(t);
		}

		mpfr_float_1000 max = mpfr_float_1000(0.0);
		mpfr_float_1000 min = std::numeric_limits<mpfr_float_1000>::max();
		mpfr_float_1000 sum = mpfr_float_1000(0.0);
		for(auto & e : upTimes)
		{
			if(e > max) max = e;
			if(e < min) min = e;
			sum += e;
		}

		maxUpTime = max;
		minUpTime = min;
		avUpTime = sum / upTimes.size();
		allUpTime = sum;
	}

	void print()
	{
		std::cout << "time: " << allTimeInt << "; up time: " << allUpTime << "; breaks: " << breaks << std::endl;
		std::cout << "max up time: " << maxUpTime << "; min up time: " << minUpTime << "; average up time: " << avUpTime << std::endl;
		std::cout << "max propagation time: " << maxPropTime << std::endl;

		if(intTimes.size() != upTimes.size()) std::cout << "ERROR " << __func__ << "size mismatch!" << std::endl;

		for(unsigned int i = 0; i < upTimes.size(); i++)
		{
			std::cout << "time interval: " << intTimes[i].first << "; up time: " << upTimes[i] << "; not connected: " << disconnected[i] << "; max path: " << intTimes[i].second << "; propagation time: " << propTime[i] << std::endl;
		}
	}

	void write(int chance)
	{
		// out file
		std::ofstream outFile;
		std::time_t now = std::time(nullptr);
		std::ostringstream oss;
		oss << "out_" << chance << "_" << now << ".txt";
		std::string name = oss.str();
		outFile.open(name);

		outFile << "time: " << allTimeInt << "; up time: " << allUpTime << "; breaks: " << breaks << std::endl;
		outFile << "max up time: " << maxUpTime << "; min up time: " << minUpTime << "; average up time: " << avUpTime << std::endl;
		outFile << "max propagation time: " << maxPropTime << std::endl;

		if(intTimes.size() != upTimes.size()) std::cout << "ERROR " << __func__ << "size mismatch!" << std::endl;

		for(unsigned int i = 0; i < upTimes.size(); i++)
		{
			outFile << "time interval: " << intTimes[i].first << "; up time: " << upTimes[i] << "; not connected: " << disconnected[i] << "; max path: " << intTimes[i].second << "; propagation time: " << propTime[i] << std::endl;
		}

		outFile.close();
	}
};


class visibilityVertex
{
public:
	unsigned int id;
	Category::SatType st_e;
	std::string st_s;
	Category::NodeType nt_e;
	std::string nt_s;
	Sl_MPFR_f_1000 sl;

	// hold vertices and the distance to it
	std::vector<std::pair<visibilityVertex, mpfr_float_1000>> visible;
	std::vector<std::pair<visibilityVertex, mpfr_float_1000>> not_visible;
	/*
	 * priority to be added to the graph: how many members does the node see.
	 * lower the number - higher the priority.
	 */
	int priority = -1;
	unsigned int intf = 0;

	void setIntf()
	{
		// GEO
		if(st_e == Category::GEO) this->intf = 8;
		else this->intf = 1;
	}

	void setPriority()
	{
		priority = visible.size();
	}

	bool doesVertexExists(const unsigned int id, const std::vector<std::pair<visibilityVertex, mpfr_float_1000>> &vec)
	{
		auto it = find_if(vec.begin(), vec.end(), [&id](const std::pair<visibilityVertex, mpfr_float_1000>& obj) { return (obj).first.id == id; });

	    if(it == vec.end())
	    {
	        return false;
	    }
	    return true;
	}
};


class Vertex
{
public:
	unsigned int id;

	// Adjacency list
	std::vector<unsigned int> adjl;

	void print()
	{
		std::cout << id << ": ";
		for(auto &e : adjl)
		{
			std::cout << e << "; ";
		}
		std::cout << std::endl;
	}
};


class Graph
{
public:
	Graph(){std::srand(std::time(0));};

	void run(int chance);
	/*
	 * for each Member satellites find all other nodes that are visible (link can be created),
	 * for each Guest satellites find all Member nodes that are visible (link can be created)
	 */
	void setVG();
	void printVG();
	void debugVG();
	void setG();
	std::set<std::pair<unsigned int, unsigned int>> lostLinks();
	void printG();

	int notConnected();
	mpfr_float_1000 maxPath();
	void randLinkBreak(int chance);

	// the satellite constellation
	Constellation constellation;
	// graph that represents which vertices are mutually visible
	std::vector<visibilityVertex> visibilityGraph;
	// the actual linked graph (8 OCT per sat)
	std::vector<Vertex> graph;

};

#endif // GRPAH_HPP
