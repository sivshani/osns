#ifndef NETWORK_HPP
#define NETWORK_HPP

#include <vector>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <random>
#include "boost/multiprecision/mpfr.hpp"
#include <Node.hpp>
#include <Vectors.hpp>

using namespace boost::math;
using namespace boost::multiprecision;


class Constellation
{
public:
	Constellation(){};

	void printNodes();
	void debug();

	void setConstellation();

	// determine if 2 satellites are visible to each other
	bool areVisible(Node<mpfr_float_1000> n1, Node<mpfr_float_1000> n2, mpfr_float_1000 &d);

	// determines if 2 satellites are in range
	bool areInRange(vec::Vector3MPFR_f_1000 p1, vec::Vector3MPFR_f_1000 p2, mpfr_float_1000 &d);

	/*
	 * determines if line intersect with a sphere.
	 * take 2 3d points (line start and end)and sphere center and radius
	 * source:
	 * http://paulbourke.net/geometry/circlesphere/raysphere.c
	 * http://www.ambrsoft.com/TrigoCalc/Sphere/SpherLineIntersection_.htm
	 */
	bool raySphere(vec::Vector3MPFR_f_1000 p1, vec::Vector3MPFR_f_1000 p2, vec::Vector3d sc = Earth::earthCenterCartesian.XYZ, mpfr_float_1000 r = Earth::radiusAthmosphere);

	/*
	 * determine if a GS is in the cone of a GEO
	 * https://stackoverflow.com/questions/12826117/how-can-i-detect-if-a-point-is-inside-a-cone-or-not-in-3d-space
	 */
	bool isInCone(Node<mpfr_float_1000> GEO, Node<mpfr_float_1000> GS);

	// move, takes seconds
	void move(mpfr_float_1000 seconds);

	// The Constellation
	std::vector<Node<mpfr_float_1000>> nodes;
};

#endif /* NETWORK_HPP */
